# -*- coding: utf-8 -*-
from jobstream.constants import FEED_LEGACY, FEED_V2, STREAM_LEGACY, STREAM_V2

from tests.test_resources.jobstream_helper import get_feed, get_stream_with_endpoint, validate_feed


def run_test_case(test_case: dict, endpoint: str) -> dict:
    params = test_case["params"]
    expected = test_case["jobstream"]
    if endpoint in [STREAM_V2, STREAM_LEGACY]:
        response = get_stream_with_endpoint(endpoint, params)
        number_of_hits = len(response)
    elif endpoint in [FEED_V2, FEED_LEGACY]:
        response = get_feed(endpoint, params)
        number_of_hits = validate_feed(response, expected)
    else:
        raise ValueError(f"Unknown endpoint: {endpoint}")
    error_msg = f"ERROR: Actual number of hits: {number_of_hits}. Expected: {expected}. Test case {test_case}"
    assert number_of_hits == expected, error_msg
    return response
