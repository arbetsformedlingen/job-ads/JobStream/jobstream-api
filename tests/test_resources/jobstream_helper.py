# -*- coding: utf-8 -*-
import json

import requests
import xmltodict
from jobstream import settings
from jobstream.constants import (
    FEED_LEGACY,
    FEED_V2,
    SNAPSHOT_LEGACY,
    SNAPSHOT_V2,
    STREAM_LEGACY,
    STREAM_V2,
)

from tests.test_resources.test_settings import NUMBER_OF_ADS, TEST_URL


# snapshot
def get_snapshot_with_headers(endpoint, headers):
    response = requests.get(f"{TEST_URL}/{endpoint}", headers=headers)
    return _check_ok_response_and_number_of_ads(response, expected_number=NUMBER_OF_ADS)


def get_snapshot(endpoint: str) -> list:
    if endpoint in [SNAPSHOT_V2, SNAPSHOT_LEGACY]:
        response = requests.get(f"{TEST_URL}/{endpoint}")
        return _check_ok_response_and_number_of_ads(response, expected_number=NUMBER_OF_ADS)
    else:
        return get_streaming_snapshot()


def get_streaming_snapshot() -> list:
    with requests.get(f"{TEST_URL}/{SNAPSHOT_V2}", stream=True) as r:
        ads = read_stream_from_response(r)
    return ads


# Stream
def get_stream_with_endpoint(endpoint, params):
    if endpoint in [STREAM_V2, STREAM_LEGACY]:
        return _get_stream(endpoint, params)
    return Exception("Endpoint not supported")


def get_stream_jsonl_with_endpoint(endpoint, params):
    if endpoint in [STREAM_V2, STREAM_LEGACY]:
        return get_stream_jsonl(endpoint, params)
    return Exception("Endpoint not supported")


def _get_stream(endpoint, params):
    response = requests.get(f"{TEST_URL}/{endpoint}", params=params)
    return _check_ok_return_content(response)


def get_stream_with_headers(expected_number, params, headers, endpoint):
    response = requests.get(f"{TEST_URL}/{endpoint}", params=params, headers=headers)
    return _check_ok_response_and_number_of_ads(response, expected_number)


def get_stream_jsonl(endpoint, params):
    headers = {"Accept": "application/jsonl"}
    with requests.get(f"{TEST_URL}/{endpoint}", params=params, headers=headers) as r:
        ads = read_stream_from_response(r)
    return ads


def get_stream_expect_error(params, expected_http_code, endpoint):
    r = requests.get(f"{TEST_URL}/{endpoint}", params=params)
    status = r.status_code
    assert status == expected_http_code, f"Expected http return code to be {expected_http_code} , but got {status}"


# Feed


def get_feed_raw(endpoint, params):
    return requests.get(f"{TEST_URL}/{endpoint}", params=params)


def get_feed(endpoint, params):
    response = get_feed_raw(endpoint, params)
    response.raise_for_status()
    return response.content.decode("utf8")


# Common
def get_ads_from_endpoint(test_case: dict, endpoint: str) -> dict:
    """
    Gets ads from either stream or feed endpoint
    :param test_case: parameters for input
    :param endpoint: endpoint to get ads from
    :return: found ads
    """
    params = test_case["params"]
    if endpoint in [STREAM_V2, STREAM_LEGACY]:
        response = get_stream_with_endpoint(endpoint, params)
    elif endpoint in [FEED_V2, FEED_LEGACY]:
        response = get_feed(endpoint, params)
    else:
        raise ValueError(f"Unknown endpoint: {endpoint}")
    return response


def read_stream_from_response(response) -> list:
    ads = []
    response.raise_for_status()
    for line in response.iter_lines():
        ads.append(json.loads(line.decode("utf-8")))
    return ads


def _check_ok_response_and_number_of_ads(response, expected_number):
    response.raise_for_status()
    assert response.content is not None
    list_of_ads = json.loads(response.content.decode("utf8"))
    if expected_number is not None:
        assert len(list_of_ads) == expected_number, f"expected {expected_number} but got {len(list_of_ads)}"
    _check_list_of_ads(list_of_ads)
    return list_of_ads


def _check_ok_return_content(response):
    response.raise_for_status()
    return json.loads(response.content.decode("utf8"))


def _check_list_of_ads(list_of_ads):
    for ad in list_of_ads:
        assert isinstance(ad["id"], str)
        checks = []
        checks.append(ad["id"])
        if not ad["removed"]:
            checks.append(ad["headline"])
            checks.append(ad["description"])
            checks.append(ad["occupation"])
            checks.append(ad["workplace_address"]["country"])
            for c in checks:
                assert c is not None, ad


def validate_feed(xml_str: str, expected_number: int):
    xml_feed = _parse_feed(xml_str)
    entries = []
    try:
        entries = xml_feed["entry"]
    except KeyError:
        pass
    if entries:
        assert len(entries) == expected_number, f"Got {len(entries)} entries, expected {expected_number}"
    for entry in entries:
        validate_single_entry(entry)
    return len(entries)


def validate_single_entry(entry: dict):
    assert len(entry) == 6, f"Entry length was {len(entry)}"
    id_len = len(f"{settings.JOBSEARCH_HOST}/ad/00000000")
    assert len(entry["id"]) == id_len, f"expected id length to be {id_len} but it was {len(entry['id'])}"
    assert entry["title"]
    assert entry["updated"]
    assert entry["published"]
    assert entry["content"]
    assert entry["content"]["@type"] == "html"
    assert entry["content"]["#text"]
    assert entry["municipality"] or entry["municipality"] == None


def validate_feed_single_entry(xml_str: str):
    xml_feed = _parse_feed(xml_str)
    try:
        validate_single_entry(xml_feed["entry"])
    except KeyError:
        pass


def validate_feed_header(xml_str: str):
    xml_feed = _parse_feed(xml_str)
    for key in ["@xmlns", "id", "title", "updated", "link", "generator", "subtitle"]:
        assert xml_feed.get(key, False)


def _parse_feed(xml_str: str) -> dict:
    return xmltodict.parse(xml_str)["feed"]
