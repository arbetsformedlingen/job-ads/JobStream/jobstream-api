# -*- coding: utf-8 -*-
import datetime
import os

from jobstream import constants

# environment variables must be set
TEST_USE_STATIC_DATA = os.getenv("TEST_USE_STATIC_DATA", True)

NUMBER_OF_ADS = 5029
DAWN_OF_TIME = "2020-01-01T00:00:01"
current_time_stamp = datetime.datetime.now().strftime(constants.DATE_FORMAT)

test_headers = {"accept": "application/json"}


TEST_URL = os.getenv("TEST_URL", "http://127.0.0.1:5000")
