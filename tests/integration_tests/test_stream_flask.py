# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import json

import pytest
from jobstream import create_app, constants

from jobstream.constants import (
    DATE_FORMAT,
    FEED_LEGACY,
    FEED_V2,
    LOCATION_CONCEPT_ID,
    OCCUPATION_CONCEPT_ID,
    STREAM_LEGACY,
    STREAM_V2,
    UPDATED_AFTER,
    UPDATED_BEFORE,
    DATE,
    UPDATED_BEFORE_DATE
)
from requests.status_codes import codes

ENDPOINTS = [STREAM_V2, FEED_V2]
LEGACY_ENDPOINTS = [STREAM_LEGACY, FEED_LEGACY]

default_time = (datetime.now() - timedelta(minutes=10)).strftime(constants.DATE_FORMAT)

app = create_app()


@pytest.fixture()
def app_fixture():
    app.config.update({"TESTING": True})
    yield app


@pytest.fixture()
def client(app_fixture):
    return app.test_client()


def response_data(response) -> str:
    return response.data.decode().strip()


def now():
    return datetime.now().strftime(DATE_FORMAT)


def _check_error(response, expected_code: int, expected_message: str) -> bool | AssertionError:
    assert response.status_code == expected_code, f"Expected {expected_code} but got {response.status_code}"
    assert response_data(response) == expected_message, f"Expected {expected_message} but got {response_data(response)}"
    return True


def _check_input_validation_error(response, expected_code: int, expected_message: str) -> bool | AssertionError:
    assert response.status_code == expected_code, f"Expected {expected_code} but got {response.status_code}"
    response_data_json = json.loads(response.data.decode())
    assert (
        response_data_json["errors"]["updated-after"]
        == "Missing required parameter in the JSON body or the post body or the query string"
    )
    assert response_data_json["message"] == "Input payload validation failed"
    return True


@pytest.mark.parametrize("endpoint", ENDPOINTS)
def test_input_validation_failed(client, endpoint):
    response = client.get(f"/{endpoint}")
    expected_message = '{"errors": {"updated-after": "Missing required parameter in the JSON body or the post body or the query string"}, "message": "Input payload validation failed"}'
    _check_input_validation_error(response, codes.bad_request, expected_message)


def test_root(client):
    response = client.get("/")
    assert response.status_code == codes.ok
    data = response_data(response)
    assert "<title>JobStream</title>" in data


def test_not_found(client):
    response = client.get("/not-found")
    assert response.status_code == codes.not_found
    data = response_data(response)
    assert "<title>404 Not Found</title>" in data


def _check_params_response(response, params, endpoint) -> bool | AssertionError:
    assert response.status_code == codes.ok
    response_endpoint, query_string = response.request.full_path.split("?")
    assert response_endpoint == f"/{endpoint}"
    assert response.request.method == "GET"
    for key, value in params.items():
        assert str(key) in query_string
        assert str(value) in query_string
    return True


@pytest.mark.parametrize("date_after", [default_time])
@pytest.mark.parametrize("use_updated_before", [True, False])
@pytest.mark.parametrize("use_location_concept_id", [True, False])
@pytest.mark.parametrize("use_occupation_concept_id", [True, False])
@pytest.mark.parametrize("endpoint", ENDPOINTS)
def test_params(client, endpoint, date_after, use_updated_before, use_location_concept_id, use_occupation_concept_id):
    """
    Test all allowed parameters and check that response is as expected

    """
    params = {UPDATED_AFTER: default_time}
    if use_updated_before:
        params.update({UPDATED_BEFORE: now()})
    if use_location_concept_id:
        params.update({LOCATION_CONCEPT_ID: "Location_01"})
    if use_occupation_concept_id:
        params.update({OCCUPATION_CONCEPT_ID: "Occupation_01"})

    response = client.get(f"/{endpoint}", query_string=params)
    _check_params_response(response, params, endpoint)


@pytest.mark.parametrize("date_after", [default_time])
@pytest.mark.parametrize("use_updated_before", [True, False])
@pytest.mark.parametrize("use_location_concept_id", [True, False])
@pytest.mark.parametrize("use_occupation_concept_id", [True, False])
@pytest.mark.parametrize("endpoint", LEGACY_ENDPOINTS)
def test_legacy_params(client, endpoint, date_after, use_updated_before, use_location_concept_id, use_occupation_concept_id):
    """
    Test all allowed parameters for the legacy endpoint and check that response is as expected
    """
    params = {DATE: date_after}
    if use_updated_before:
        params.update({UPDATED_BEFORE_DATE: now()})
    if use_location_concept_id:
        params.update({LOCATION_CONCEPT_ID: "Location_01"})
    if use_occupation_concept_id:
        params.update({OCCUPATION_CONCEPT_ID: "Occupation_01"})

    response = client.get(f"/{endpoint}", query_string=params)
    _check_params_response(response, params, endpoint)


@pytest.mark.parametrize("endpoint", ENDPOINTS)
def test_unknown_param_is_ignored(client, endpoint):
    """
    Unknown parameters are ignored
    """
    params = {UPDATED_AFTER: default_time, "unknown-param": True}
    response = client.get(f"/{endpoint}", query_string=params)
    _check_params_response(response, params, endpoint)


@pytest.mark.parametrize("endpoint", ENDPOINTS)
def test_method_not_allowed(client, endpoint):
    """
    Runs through all methods that will cause http 405 / Method not allowed
    If any of them fails, they will be added to a list that is printed at the end when the
    test fails with an AssertionError
    """
    methods_not_allowed = [client.post, client.put, client.delete, client.patch]
    failures = []
    for method in methods_not_allowed:
        try:
            response = method(f"/{endpoint}")
            assert json.loads(response.data.decode())["message"] == "The method is not allowed for the requested URL."
        except:  # noqa: E722
            method_name = repr(method).split("<")[1].split("method")[1].replace("of", "").strip()
            failures.append(method_name)

    assert not failures, f"Errors: {failures}"
