# -*- coding: utf-8 -*-
import pytest
from jobstream.constants import STREAM_LEGACY, STREAM_V2

import tests.test_resources.ad_fields_data_type as type_check
from tests.test_resources.jobstream_helper import get_stream_jsonl_with_endpoint, get_stream_with_endpoint

"""
This will ony get a limited number of ads from the stream endpoint to speed up test execution for smoke tests
"""
# all tests in this file are smoke tests
pytestmark = pytest.mark.smoke

@pytest.mark.parametrize("endpoint", [STREAM_V2])
def test_contract(endpoint):
    ads = get_stream_with_endpoint(endpoint, params={"updated-after": "2021-03-25T12:30:40"})
    for ad in ads:
        type_check.check_types_top_level(ad)
        type_check.check_types_occupation(ad)
        type_check.check_duration(ad)
        type_check.check_employment_type(ad)
        type_check.check_salary_type(ad)
        type_check.check_types_employer(ad)
        type_check.check_scope_of_work(ad)
        type_check.check_application_details(ad)
        type_check.check_job_ad_description(ad)
        type_check.check_workplace_address(ad)
        type_check.check_requirements(ad)
        type_check.check_working_hours_type(ad)
        type_check.check_types_contact(ad)
        type_check.check_length(ad, 36)

@pytest.mark.parametrize("endpoint", [STREAM_LEGACY])
def test_contract_legacy(endpoint):
    ads = get_stream_with_endpoint(endpoint, params={"date": "2021-03-25T12:30:40"})
    for ad in ads:
        type_check.check_types_top_level(ad)
        type_check.check_types_occupation(ad)
        type_check.check_duration(ad)
        type_check.check_employment_type(ad)
        type_check.check_salary_type(ad)
        type_check.check_types_employer(ad)
        type_check.check_scope_of_work(ad)
        type_check.check_application_details(ad)
        type_check.check_job_ad_description(ad)
        type_check.check_workplace_address(ad)
        type_check.check_requirements(ad)
        type_check.check_working_hours_type(ad)
        type_check.check_types_contact(ad)
        type_check.check_length(ad, 36)

@pytest.mark.parametrize("endpoint", [STREAM_V2])
def test_contract_jsonl(endpoint):
    ads = get_stream_jsonl_with_endpoint(endpoint, params={"updated-after": "2021-03-25T12:30:40"})
    for ad in ads:
        type_check.check_types_top_level(ad)
        type_check.check_types_occupation(ad)
        type_check.check_duration(ad)
        type_check.check_employment_type(ad)
        type_check.check_salary_type(ad)
        type_check.check_types_employer(ad)
        type_check.check_scope_of_work(ad)
        type_check.check_application_details(ad)
        type_check.check_job_ad_description(ad)
        type_check.check_workplace_address(ad)
        type_check.check_requirements(ad)
        type_check.check_working_hours_type(ad)
        type_check.check_types_contact(ad)
        type_check.check_length(ad, 36)
