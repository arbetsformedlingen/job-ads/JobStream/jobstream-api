# -*- coding: utf-8 -*-
import logging
from datetime import datetime

import pytest
from jobstream import constants
from jobstream.constants import FEED_LEGACY, FEED_V2, STREAM_LEGACY, STREAM_V2
from pytz import timezone

from tests.test_resources.jobstream_api_test_runner import run_test_case
from tests.test_resources.jobstream_helper import get_ads_from_endpoint
from tests.test_resources.test_settings import current_time_stamp

"""
These test cases will run against /stream and /feed
"""
log = logging.getLogger(__name__)

# geographical concept ids used in this file
varmlands_lan = "EVVp_h6U_GSZ"
karlstad = "hRDj_PoV_sFU"
kristinehamn = "SVQS_uwJ_m2B"
grums = "PSNt_P95_x6q"

# Occupation concept ids used in this file
sjukskoterska__grundutbildad = "bXNH_MNX_dUR"
sjukskoterska__medicin_och_kirurgi = "4KeX_GcW_9jY"
field_halso__och_sjukvard = "NYW6_mP6_vwf"
group_ambulanssjukskoterskor_m_fl_ = "PHJN_fva_yxs"
group_grundutbildade_sjukskoterskor = "Z8ci_bBE_tmx"


STREAM_AND_FEED_ENDPOINTS = [FEED_V2, STREAM_V2]
LEGACY_STREAM_AND_FEED_ENDPOINTS = [FEED_LEGACY, STREAM_LEGACY]

STREAM_ENDPOINT = [STREAM_V2]
LEGACY_STREAM_ENDPOINT= [STREAM_LEGACY]

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "fsnw_ZCu_v2U"}, "jobstream": 22},
        pytest.param(
            {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "TMsM_oNw_j6z"}, "jobstream": 8},
            marks=pytest.mark.smoke,
        ),
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "DJh5_yyF_hEM"}, "jobstream": 239},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 50},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "heGV_uHh_o8W"}, "jobstream": 28},
    ],
)
def test_filter_only_on_occupation(test_case, endpoint):
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "fsnw_ZCu_v2U"}, "jobstream": 22},
        pytest.param(
            {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "TMsM_oNw_j6z"}, "jobstream": 8},
            marks=pytest.mark.smoke,
        ),
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "DJh5_yyF_hEM"}, "jobstream": 239},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 50},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "heGV_uHh_o8W"}, "jobstream": 28},
    ],
)
def test_filter_only_on_occupation_legacy(test_case, endpoint):
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "QJgN_Zge_BzJ"}, "jobstream": 10},
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "9QUH_2bb_6Np"}, "jobstream": 140},
        pytest.param(
            {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "CCVZ_JA7_d3y"}, "jobstream": 20},
            marks=pytest.mark.smoke,
        ),
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 1227},
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "AvNB_uwa_6n6"}, "jobstream": 775},
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4999},
    ],
)
def test_filter_only_on_location(test_case, endpoint):
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "QJgN_Zge_BzJ"}, "jobstream": 10},
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "9QUH_2bb_6Np"}, "jobstream": 140},
        pytest.param(
            {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "CCVZ_JA7_d3y"}, "jobstream": 20},
            marks=pytest.mark.smoke,
        ),
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 1227},
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "AvNB_uwa_6n6"}, "jobstream": 775},
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4999},
    ],
)
def test_filter_only_on_location_legacy(test_case, endpoint):
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": "wHUj_vsP_Jhn",
                "location-concept-id": "zdoY_6u5_Krt",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "updated-after": "2020-12-15T00:00:01",
                "occupation-concept-id": "wHUj_vsP_Jhn",
                "location-concept-id": "zdoY_6u5_Krt",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": "dKVR_7ew_sCA",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 7,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": "qSXj_aXc_EGp",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 3,
        },
        pytest.param(
            {
                "params": {
                    "updated-after": "2020-12-01T00:00:01",
                    "occupation-concept-id": "iYgm_hfD_Bez",
                    "location-concept-id": "CifL_Rzy_Mku",
                },
                "jobstream": 10,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": "Z8ci_bBE_tmx",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 393,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": "DJh5_yyF_hEM",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 237,
        },
        {
            "params": {
                "updated-after": "2020-11-01T00:00:01",
                "occupation-concept-id": "DJh5_yyF_hEM",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 236,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": "bH5L_uXD_ZAX",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 965,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 195,
        },
        {
            "params": {
                "updated-after": "2020-11-25T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 958,
        },
        {
            "params": {
                "updated-after": "2020-12-15T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 192,
        },
    ],
)
def test_filter_with_date_and_occupation_and_location(test_case, endpoint):
    """
    should return results based on date AND occupation type AND location
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": "wHUj_vsP_Jhn",
                "location-concept-id": "zdoY_6u5_Krt",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "date": "2020-12-15T00:00:01",
                "occupation-concept-id": "wHUj_vsP_Jhn",
                "location-concept-id": "zdoY_6u5_Krt",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": "dKVR_7ew_sCA",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 7,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": "qSXj_aXc_EGp",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 3,
        },
        pytest.param(
            {
                "params": {
                    "date": "2020-12-01T00:00:01",
                    "occupation-concept-id": "iYgm_hfD_Bez",
                    "location-concept-id": "CifL_Rzy_Mku",
                },
                "jobstream": 10,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": "Z8ci_bBE_tmx",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 393,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": "DJh5_yyF_hEM",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 237,
        },
        {
            "params": {
                "date": "2020-11-01T00:00:01",
                "occupation-concept-id": "DJh5_yyF_hEM",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 236,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": "bH5L_uXD_ZAX",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 965,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 195,
        },
        {
            "params": {
                "date": "2020-11-25T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 958,
        },
        {
            "params": {
                "date": "2020-12-15T00:00:01",
                "occupation-concept-id": "NYW6_mP6_vwf",
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 192,
        },
    ],
)
def test_filter_with_date_and_occupation_and_location_legacy(test_case, endpoint):
    """
    should return results based on date AND occupation type AND location
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 181},
        {"params": {"updated-after": "2020-11-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 181},
        {"params": {"updated-after": "2020-12-15T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 179},
        {"params": {"updated-after": "2021-01-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 177},
        {"params": {"updated-after": "2021-03-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 122},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "j8X2_KhG_6ty"}, "jobstream": 7},
        {"params": {"updated-after": "2020-12-01T00:00:01", "occupation-concept-id": "j8X2_KhG_6ty"}, "jobstream": 7},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 50},
        pytest.param(
            {"params": {"updated-after": "2020-10-25T00:00:00", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 49},
            marks=pytest.mark.smoke,
        ),
        {"params": {"updated-after": "2020-11-25T00:00:00", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 47},
        {"params": {"updated-after": "2020-12-15T00:00:00", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 46},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "fsnw_ZCu_v2U"}, "jobstream": 22},
        {"params": {"updated-after": "2021-02-25T00:00:00", "occupation-concept-id": "fsnw_ZCu_v2U"}, "jobstream": 19},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "bH5L_uXD_ZAX"}, "jobstream": 5},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "GazW_2TU_kJw"}, "jobstream": 347},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "1ERY_48v_Q6m"}, "jobstream": 4},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "8jrL_5RZ_9Kt"}, "jobstream": 42},
        {"params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "JjFi_LiK_yR1"}, "jobstream": 2},
    ],
)
def test_filter_with_date_and_one_occupation(test_case, endpoint):
    """
    test of filtering in /stream: should return results based on date AND occupation-related concept_id
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 181},
        {"params": {"date": "2020-11-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 181},
        {"params": {"date": "2020-12-15T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 179},
        {"params": {"date": "2021-01-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 177},
        {"params": {"date": "2021-03-01T00:00:01", "occupation-concept-id": "eU1q_zvL_9Rf"}, "jobstream": 122},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "j8X2_KhG_6ty"}, "jobstream": 7},
        {"params": {"date": "2020-12-01T00:00:01", "occupation-concept-id": "j8X2_KhG_6ty"}, "jobstream": 7},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 50},
        pytest.param(
            {"params": {"date": "2020-10-25T00:00:00", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 49},
            marks=pytest.mark.smoke,
        ),
        {"params": {"date": "2020-11-25T00:00:00", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 47},
        {"params": {"date": "2020-12-15T00:00:00", "occupation-concept-id": "rQds_YGd_quU"}, "jobstream": 46},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "fsnw_ZCu_v2U"}, "jobstream": 22},
        {"params": {"date": "2021-02-25T00:00:00", "occupation-concept-id": "fsnw_ZCu_v2U"}, "jobstream": 19},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "bH5L_uXD_ZAX"}, "jobstream": 5},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "GazW_2TU_kJw"}, "jobstream": 347},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "1ERY_48v_Q6m"}, "jobstream": 4},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "8jrL_5RZ_9Kt"}, "jobstream": 42},
        {"params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": "JjFi_LiK_yR1"}, "jobstream": 2},
    ],
)
def test_filter_with_date_and_one_occupation_legacy(test_case, endpoint):
    """
    test of filtering in /stream: should return results based on date AND occupation-related concept_id
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "N1wJ_Cuu_7Cs"}, "jobstream": 33},
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 1227},
        {"params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 1211},
        {"params": {"updated-after": "2021-03-05T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 833},
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "QJgN_Zge_BzJ"}, "jobstream": 10},
        {"params": {"updated-after": "2021-01-01T00:00:01", "location-concept-id": "wjee_qH2_yb6"}, "jobstream": 136},
        pytest.param(
            {"params": {"updated-after": "2021-01-01T00:00:01", "location-concept-id": "bm2x_1mr_Qhx"}, "jobstream": 103},
            marks=pytest.mark.smoke,
        ),
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4999},
        {"params": {"updated-after": "2020-11-01T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4981},
        {"params": {"updated-after": "2020-12-15T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4914},
        {"params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": "AvNB_uwa_6n6"}, "jobstream": 762},
        {"params": {"updated-after": "2021-02-01T00:00:01", "location-concept-id": "QJgN_Zge_BzJ"}, "jobstream": 8},
        {"params": {"updated-after": "2020-11-01T00:00:01", "location-concept-id": "oDpK_oZ2_WYt"}, "jobstream": 121},
        {"params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": "oDpK_oZ2_WYt"}, "jobstream": 118},
        {"params": {"updated-after": "2021-02-15T00:00:01", "location-concept-id": "oDpK_oZ2_WYt"}, "jobstream": 99},
    ],
)
def test_filter_with_date_and_location(test_case, endpoint):
    """
    should return results based on date AND occupation type AND location_1
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "N1wJ_Cuu_7Cs"}, "jobstream": 33},
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 1227},
        {"params": {"date": "2020-12-01T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 1211},
        {"params": {"date": "2021-03-05T00:00:01", "location-concept-id": "CifL_Rzy_Mku"}, "jobstream": 833},
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "QJgN_Zge_BzJ"}, "jobstream": 10},
        {"params": {"date": "2021-01-01T00:00:01", "location-concept-id": "wjee_qH2_yb6"}, "jobstream": 136},
        pytest.param(
            {"params": {"date": "2021-01-01T00:00:01", "location-concept-id": "bm2x_1mr_Qhx"}, "jobstream": 103},
            marks=pytest.mark.smoke,
        ),
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4999},
        {"params": {"date": "2020-11-01T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4981},
        {"params": {"date": "2020-12-15T00:00:01", "location-concept-id": "i46j_HmG_v64"}, "jobstream": 4914},
        {"params": {"date": "2020-12-01T00:00:01", "location-concept-id": "AvNB_uwa_6n6"}, "jobstream": 762},
        {"params": {"date": "2021-02-01T00:00:01", "location-concept-id": "QJgN_Zge_BzJ"}, "jobstream": 8},
        {"params": {"date": "2020-11-01T00:00:01", "location-concept-id": "oDpK_oZ2_WYt"}, "jobstream": 121},
        {"params": {"date": "2020-12-01T00:00:01", "location-concept-id": "oDpK_oZ2_WYt"}, "jobstream": 118},
        {"params": {"date": "2021-02-15T00:00:01", "location-concept-id": "oDpK_oZ2_WYt"}, "jobstream": 99},
    ],
)
def test_filter_with_date_and_location_legacy(test_case, endpoint):
    """
    should return results based on date AND occupation type AND location_1
    """
    run_test_case(test_case, endpoint)

#   multiple params of same type
@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": ["8jrL_5RZ_9Kt", "JjFi_LiK_yR1"]},
            "jobstream": 44,
        },
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": ["rQds_YGd_quU", "fsnw_ZCu_v2U"]},
            "jobstream": 72,
        },
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": ["JjFi_LiK_yR1", "8jrL_5RZ_9Kt"]},
            "jobstream": 44,
        },
        {
            "params": {"updated-after": "2020-11-01T00:00:01", "occupation-concept-id": ["rQds_YGd_quU", "fsnw_ZCu_v2U"]},
            "jobstream": 71,
        },
        pytest.param(
            {
                "params": {"updated-after": "2020-12-01T00:00:01", "occupation-concept-id": ["rQds_YGd_quU", "fsnw_ZCu_v2U"]},
                "jobstream": 69,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": ["1ERY_48v_Q6m", "bH5L_uXD_ZAX"]},
            "jobstream": 9,
        },
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": ["bH5L_uXD_ZAX", "1ERY_48v_Q6m"]},
            "jobstream": 9,
        },
        {
            "params": {"updated-after": "2020-11-01T00:00:01", "occupation-concept-id": ["5qT8_z9d_8rw", "XFXX_Jq9_Y2a"]},
            "jobstream": 6,
        },
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": ["5qT8_z9d_8rw", "XFXX_Jq9_Y2a"]},
            "jobstream": 6,
        },
        {
            "params": {"updated-after": "2020-11-11T00:00:01", "occupation-concept-id": ["TMsM_oNw_j6z", "apaJ_2ja_LuF"]},
            "jobstream": 422,
        },
        {
            "params": {"updated-after": "2020-12-12T00:00:01", "occupation-concept-id": ["TMsM_oNw_j6z", "apaJ_2ja_LuF"]},
            "jobstream": 412,
        },
        {
            "params": {"updated-after": "2021-01-25T00:00:00", "occupation-concept-id": ["LyBp_bY6_bZK", "R2kh_8rW_qbm"]},
            "jobstream": 11,
        },
        {
            "params": {"updated-after": "2020-11-25T00:00:00", "occupation-concept-id": ["MVqp_eS8_kDZ", "NYW6_mP6_vwf"]},
            "jobstream": 1488,
        },
        {
            "params": {"updated-after": "2020-12-15T00:00:00", "occupation-concept-id": ["PaxQ_o1G_wWH", "xr7w_N6Q_QMA"]},
            "jobstream": 21,
        },
        {
            "params": {"updated-after": "2020-11-25T00:00:00", "occupation-concept-id": ["GazW_2TU_kJw", "rz2m_96d_vyF"]},
            "jobstream": 349,
        },
    ],
)
def test_filter_with_date_and_two_occupations(test_case, endpoint):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on both date AND (work_1 OR work_2)
    """
    run_test_case(test_case, endpoint)

#   multiple params of same type
@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": ["8jrL_5RZ_9Kt", "JjFi_LiK_yR1"]},
            "jobstream": 44,
        },
        {
            "params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": ["rQds_YGd_quU", "fsnw_ZCu_v2U"]},
            "jobstream": 72,
        },
        {
            "params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": ["JjFi_LiK_yR1", "8jrL_5RZ_9Kt"]},
            "jobstream": 44,
        },
        {
            "params": {"date": "2020-11-01T00:00:01", "occupation-concept-id": ["rQds_YGd_quU", "fsnw_ZCu_v2U"]},
            "jobstream": 71,
        },
        pytest.param(
            {
                "params": {"date": "2020-12-01T00:00:01", "occupation-concept-id": ["rQds_YGd_quU", "fsnw_ZCu_v2U"]},
                "jobstream": 69,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": ["1ERY_48v_Q6m", "bH5L_uXD_ZAX"]},
            "jobstream": 9,
        },
        {
            "params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": ["bH5L_uXD_ZAX", "1ERY_48v_Q6m"]},
            "jobstream": 9,
        },
        {
            "params": {"date": "2020-11-01T00:00:01", "occupation-concept-id": ["5qT8_z9d_8rw", "XFXX_Jq9_Y2a"]},
            "jobstream": 6,
        },
        {
            "params": {"date": "1971-01-01T00:00:01", "occupation-concept-id": ["5qT8_z9d_8rw", "XFXX_Jq9_Y2a"]},
            "jobstream": 6,
        },
        {
            "params": {"date": "2020-11-11T00:00:01", "occupation-concept-id": ["TMsM_oNw_j6z", "apaJ_2ja_LuF"]},
            "jobstream": 422,
        },
        {
            "params": {"date": "2020-12-12T00:00:01", "occupation-concept-id": ["TMsM_oNw_j6z", "apaJ_2ja_LuF"]},
            "jobstream": 412,
        },
        {
            "params": {"date": "2021-01-25T00:00:00", "occupation-concept-id": ["LyBp_bY6_bZK", "R2kh_8rW_qbm"]},
            "jobstream": 11,
        },
        {
            "params": {"date": "2020-11-25T00:00:00", "occupation-concept-id": ["MVqp_eS8_kDZ", "NYW6_mP6_vwf"]},
            "jobstream": 1488,
        },
        {
            "params": {"date": "2020-12-15T00:00:00", "occupation-concept-id": ["PaxQ_o1G_wWH", "xr7w_N6Q_QMA"]},
            "jobstream": 21,
        },
        {
            "params": {"date": "2020-11-25T00:00:00", "occupation-concept-id": ["GazW_2TU_kJw", "rz2m_96d_vyF"]},
            "jobstream": 349,
        },
    ],
)
def test_filter_with_date_and_two_occupations_legacy(test_case, endpoint):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on both date AND (work_1 OR work_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["8jrL_5RZ_9Kt", "JjFi_LiK_yR1", "rz2m_96d_vyF"],
            },
            "jobstream": 46,
        },
        {
            "params": {
                "updated-after": "2020-11-01T00:00:01",
                "occupation-concept-id": ["rQds_YGd_quU", "rz2m_96d_vyF", "fsnw_ZCu_v2U"],
            },
            "jobstream": 73,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["1ERY_48v_Q6m", "UZ5P_spz_VLt", "bH5L_uXD_ZAX"],
            },
            "jobstream": 10,
        },
        {
            "params": {
                "updated-after": "2020-11-01T00:00:01",
                "occupation-concept-id": ["5qT8_z9d_8rw", "XFXX_Jq9_Y2a", "UjoW_meY_Zwt"],
            },
            "jobstream": 36,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["TMsM_oNw_j6z", "PHJN_fva_yxs", "apaJ_2ja_LuF"],
            },
            "jobstream": 425,
        },
        pytest.param(
            {
                "params": {
                    "updated-after": "2020-11-25T00:00:00",
                    "occupation-concept-id": ["LyBp_bY6_bZK", "xr7w_N6Q_QMA", "R2kh_8rW_qbm"],
                },
                "jobstream": 16,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["MVqp_eS8_kDZ", "NYW6_mP6_vwf", "Uuf1_GMh_Uvw"],
            },
            "jobstream": 1522,
        },
        {
            "params": {
                "updated-after": "2020-11-25T00:00:00",
                "occupation-concept-id": ["MVqp_eS8_kDZ", "NYW6_mP6_vwf", "Uuf1_GMh_Uvw"],
            },
            "jobstream": 1512,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["PaxQ_o1G_wWH", "apaJ_2ja_LuF", "xr7w_N6Q_QMA"],
            },
            "jobstream": 443,
        },
        {
            "params": {
                "updated-after": "2020-11-25T00:00:00",
                "occupation-concept-id": ["PaxQ_o1G_wWH", "apaJ_2ja_LuF", "xr7w_N6Q_QMA"],
            },
            "jobstream": 429,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["GazW_2TU_kJw", "j7Cq_ZJe_GkT", "rz2m_96d_vyF"],
            },
            "jobstream": 540,
        },
        {
            "params": {
                "updated-after": "2020-11-25T00:00:00",
                "occupation-concept-id": ["GazW_2TU_kJw", "j7Cq_ZJe_GkT", "rz2m_96d_vyF"],
            },
            "jobstream": 538,
        },
    ],
)
def test_filter_with_date_and_three_occupations(test_case, endpoint):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on date AND (work_1 OR work_2 OR work 3)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["8jrL_5RZ_9Kt", "JjFi_LiK_yR1", "rz2m_96d_vyF"],
            },
            "jobstream": 46,
        },
        {
            "params": {
                "date": "2020-11-01T00:00:01",
                "occupation-concept-id": ["rQds_YGd_quU", "rz2m_96d_vyF", "fsnw_ZCu_v2U"],
            },
            "jobstream": 73,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["1ERY_48v_Q6m", "UZ5P_spz_VLt", "bH5L_uXD_ZAX"],
            },
            "jobstream": 10,
        },
        {
            "params": {
                "date": "2020-11-01T00:00:01",
                "occupation-concept-id": ["5qT8_z9d_8rw", "XFXX_Jq9_Y2a", "UjoW_meY_Zwt"],
            },
            "jobstream": 36,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["TMsM_oNw_j6z", "PHJN_fva_yxs", "apaJ_2ja_LuF"],
            },
            "jobstream": 425,
        },
        pytest.param(
            {
                "params": {
                    "date": "2020-11-25T00:00:00",
                    "occupation-concept-id": ["LyBp_bY6_bZK", "xr7w_N6Q_QMA", "R2kh_8rW_qbm"],
                },
                "jobstream": 16,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["MVqp_eS8_kDZ", "NYW6_mP6_vwf", "Uuf1_GMh_Uvw"],
            },
            "jobstream": 1522,
        },
        {
            "params": {
                "date": "2020-11-25T00:00:00",
                "occupation-concept-id": ["MVqp_eS8_kDZ", "NYW6_mP6_vwf", "Uuf1_GMh_Uvw"],
            },
            "jobstream": 1512,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["PaxQ_o1G_wWH", "apaJ_2ja_LuF", "xr7w_N6Q_QMA"],
            },
            "jobstream": 443,
        },
        {
            "params": {
                "date": "2020-11-25T00:00:00",
                "occupation-concept-id": ["PaxQ_o1G_wWH", "apaJ_2ja_LuF", "xr7w_N6Q_QMA"],
            },
            "jobstream": 429,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["GazW_2TU_kJw", "j7Cq_ZJe_GkT", "rz2m_96d_vyF"],
            },
            "jobstream": 540,
        },
        {
            "params": {
                "date": "2020-11-25T00:00:00",
                "occupation-concept-id": ["GazW_2TU_kJw", "j7Cq_ZJe_GkT", "rz2m_96d_vyF"],
            },
            "jobstream": 538,
        },
    ],
)
def test_filter_with_date_and_three_occupations_legacy(test_case, endpoint):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on date AND (work_1 OR work_2 OR work 3)
    """
    run_test_case(test_case, endpoint)


@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["heGV_uHh_o8W", "qSXj_aXc_EGp"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 31,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["heGV_uHh_o8W", "TMsM_oNw_j6z"],
                "location-concept-id": "zdoY_6u5_Krt",
            },
            "jobstream": 7,
        },
        pytest.param(
            {
                "params": {
                    "updated-after": "1971-01-01T00:00:01",
                    "occupation-concept-id": ["heGV_uHh_o8W", "Uuf1_GMh_Uvw"],
                    "location-concept-id": "i46j_HmG_v64",
                },
                "jobstream": 53,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["Z8ci_bBE_tmx", "Z8ci_bBE_tmx"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 393,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["TMsM_oNw_j6z", "UjoW_meY_Zwt"],
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "occupation-concept-id": ["TMsM_oNw_j6z", "fsnw_ZCu_v2U"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 30,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["bH5L_uXD_ZAX", "PaxQ_o1G_wWH"],
                "location-concept-id": "AvNB_uwa_6n6",
            },
            "jobstream": 4,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["bH5L_uXD_ZAX", "PHJN_fva_yxs"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 15,
        },
    ],
)
def test_filter_with_date_and_two_occupations_and_location(test_case, endpoint):
    """
    should return results based on date AND location AND (work_1 OR work_2)
    results = work_1 + work_2 that matches location
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["heGV_uHh_o8W", "qSXj_aXc_EGp"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 31,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["heGV_uHh_o8W", "TMsM_oNw_j6z"],
                "location-concept-id": "zdoY_6u5_Krt",
            },
            "jobstream": 7,
        },
        pytest.param(
            {
                "params": {
                    "date": "1971-01-01T00:00:01",
                    "occupation-concept-id": ["heGV_uHh_o8W", "Uuf1_GMh_Uvw"],
                    "location-concept-id": "i46j_HmG_v64",
                },
                "jobstream": 53,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["Z8ci_bBE_tmx", "Z8ci_bBE_tmx"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 393,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["TMsM_oNw_j6z", "UjoW_meY_Zwt"],
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 5,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "occupation-concept-id": ["TMsM_oNw_j6z", "fsnw_ZCu_v2U"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 30,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["bH5L_uXD_ZAX", "PaxQ_o1G_wWH"],
                "location-concept-id": "AvNB_uwa_6n6",
            },
            "jobstream": 4,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["bH5L_uXD_ZAX", "PHJN_fva_yxs"],
                "location-concept-id": "i46j_HmG_v64",
            },
            "jobstream": 15,
        },
    ],
)
def test_filter_with_date_and_two_occupations_and_location_legacy(test_case, endpoint):
    """
    should return results based on date AND location AND (work_1 OR work_2)
    results = work_1 + work_2 that matches location
    """
    run_test_case(test_case, endpoint)


@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": ["Jkyb_5MQ_7pB", "N1wJ_Cuu_7Cs"]},
            "jobstream": 39,
        },
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": ["Jkyb_5MQ_7pB", "CifL_Rzy_Mku"]},
            "jobstream": 1233,
        },
        pytest.param(
            {
                "params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": ["Jkyb_5MQ_7pB", "QJgN_Zge_BzJ"]},
                "jobstream": 16,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {"updated-after": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "wjee_qH2_yb6"]},
            "jobstream": 258,
        },
        {
            "params": {"updated-after": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "bm2x_1mr_Qhx"]},
            "jobstream": 230,
        },
        {
            "params": {"updated-after": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "i46j_HmG_v64"]},
            "jobstream": 4981,
        },
        {
            "params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "AvNB_uwa_6n6"]},
            "jobstream": 763,
        },
        {
            "params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "MtbE_xWT_eMi"]},
            "jobstream": 195,
        },
        {
            "params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"]},
            "jobstream": 11,
        },
        {
            "params": {"updated-after": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "Q78b_oCw_Yq2"]},
            "jobstream": 122,
        },
        {
            "params": {"updated-after": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"]},
            "jobstream": 11,
        },
    ],
)
def test_filter_with_date_and_two_locations(test_case, endpoint):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"date": "1971-01-01T00:00:01", "location-concept-id": ["Jkyb_5MQ_7pB", "N1wJ_Cuu_7Cs"]},
            "jobstream": 39,
        },
        {
            "params": {"date": "1971-01-01T00:00:01", "location-concept-id": ["Jkyb_5MQ_7pB", "CifL_Rzy_Mku"]},
            "jobstream": 1233,
        },
        pytest.param(
            {
                "params": {"date": "1971-01-01T00:00:01", "location-concept-id": ["Jkyb_5MQ_7pB", "QJgN_Zge_BzJ"]},
                "jobstream": 16,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {"date": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "wjee_qH2_yb6"]},
            "jobstream": 258,
        },
        {
            "params": {"date": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "bm2x_1mr_Qhx"]},
            "jobstream": 230,
        },
        {
            "params": {"date": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "i46j_HmG_v64"]},
            "jobstream": 4981,
        },
        {
            "params": {"date": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "AvNB_uwa_6n6"]},
            "jobstream": 763,
        },
        {
            "params": {"date": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "MtbE_xWT_eMi"]},
            "jobstream": 195,
        },
        {
            "params": {"date": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"]},
            "jobstream": 11,
        },
        {
            "params": {"date": "2020-11-01T00:00:01", "location-concept-id": ["oDpK_oZ2_WYt", "Q78b_oCw_Yq2"]},
            "jobstream": 122,
        },
        {
            "params": {"date": "2020-12-01T00:00:01", "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"]},
            "jobstream": 11,
        },
    ],
)
def test_filter_with_date_and_two_locations_legacy(test_case, endpoint):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": [varmlands_lan]}, "jobstream": 98},
        pytest.param(
            {
                "params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": [varmlands_lan, karlstad]},
                "jobstream": 98,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {"updated-after": "1971-01-01T00:00:01", "location-concept-id": [varmlands_lan, karlstad, kristinehamn]},
            "jobstream": 98,
        },
        {
            "params": {
                "updated-after": "1971-01-01T00:00:01",
                "location-concept-id": [varmlands_lan, karlstad, kristinehamn, grums],
            },
            "jobstream": 98,
        },
    ],
)
def test_filter_with_date_and_multiple_locations_in_same_region(test_case, endpoint):
    """
    The number of ads should not differ when using a region or the region and cities within that region
    Using named concept ids to make it easier to understand relations
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "1971-01-01T00:00:01", "location-concept-id": [varmlands_lan]}, "jobstream": 98},
        pytest.param(
            {
                "params": {"date": "1971-01-01T00:00:01", "location-concept-id": [varmlands_lan, karlstad]},
                "jobstream": 98,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {"date": "1971-01-01T00:00:01", "location-concept-id": [varmlands_lan, karlstad, kristinehamn]},
            "jobstream": 98,
        },
        {
            "params": {
                "date": "1971-01-01T00:00:01",
                "location-concept-id": [varmlands_lan, karlstad, kristinehamn, grums],
            },
            "jobstream": 98,
        },
    ],
)
def test_filter_with_date_and_multiple_locations_in_same_region_legacy(test_case, endpoint):
    """
    The number of ads should not differ when using a region or the region and cities within that region
    Using named concept ids to make it easier to understand relations
    """
    run_test_case(test_case, endpoint)


@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"updated-after": "2021-03-20T00:00:01", "occupation-concept-id": [field_halso__och_sjukvard]},
            "jobstream": 236,
        },
        {
            "params": {
                "updated-after": "2021-03-20T00:00:01",
                "occupation-concept-id": [field_halso__och_sjukvard, sjukskoterska__grundutbildad],
            },
            "jobstream": 236,
        },
        {
            "params": {
                "updated-after": "2021-03-20T00:00:01",
                "occupation-concept-id": [field_halso__och_sjukvard, group_grundutbildade_sjukskoterskor],
            },
            "jobstream": 236,
        },
        pytest.param(
            {
                "params": {
                    "updated-after": "2021-03-20T00:00:01",
                    "occupation-concept-id": [
                        field_halso__och_sjukvard,
                        group_grundutbildade_sjukskoterskor,
                        group_ambulanssjukskoterskor_m_fl_,
                        sjukskoterska__medicin_och_kirurgi,
                    ],
                },
                "jobstream": 236,
            },
            marks=pytest.mark.smoke,
        ),
    ],
)
def test_filter_with_date_and_multiple_occupations_within_same_field(test_case, endpoint):
    """
    Result should be the same for occupation field even if occupations or occupations within that group are added
    Using named concept ids to make it easier to understand relations
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"date": "2021-03-20T00:00:01", "occupation-concept-id": [field_halso__och_sjukvard]},
            "jobstream": 236,
        },
        {
            "params": {
                "date": "2021-03-20T00:00:01",
                "occupation-concept-id": [field_halso__och_sjukvard, sjukskoterska__grundutbildad],
            },
            "jobstream": 236,
        },
        {
            "params": {
                "date": "2021-03-20T00:00:01",
                "occupation-concept-id": [field_halso__och_sjukvard, group_grundutbildade_sjukskoterskor],
            },
            "jobstream": 236,
        },
        pytest.param(
            {
                "params": {
                    "date": "2021-03-20T00:00:01",
                    "occupation-concept-id": [
                        field_halso__och_sjukvard,
                        group_grundutbildade_sjukskoterskor,
                        group_ambulanssjukskoterskor_m_fl_,
                        sjukskoterska__medicin_och_kirurgi,
                    ],
                },
                "jobstream": 236,
            },
            marks=pytest.mark.smoke,
        ),
    ],
)
def test_filter_with_date_and_multiple_occupations_within_same_field_legacy(test_case, endpoint):
    """
    Result should be the same for occupation field even if occupations or occupations within that group are added
    Using named concept ids to make it easier to understand relations
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"updated-after": "2020-01-01T00:00:01", "occupation-concept-id": [field_halso__och_sjukvard]},
            "jobstream": 971,
        },
        {
            "params": {"updated-after": "2020-01-01T00:00:01", "occupation-concept-id": [group_grundutbildade_sjukskoterskor]},
            "jobstream": 396,
        },
        {
            "params": {"updated-after": "2020-01-01T00:00:01", "occupation-concept-id": [sjukskoterska__grundutbildad]},
            "jobstream": 392,
        },
    ],
)
def test_filter_narrowing_down_occupations_within_same_field(test_case, endpoint):
    """
    Number of ads should be fewer when searching for field, group and occupation
    Using named concept ids to make it easier to understand relations
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"date": "2020-01-01T00:00:01", "occupation-concept-id": [field_halso__och_sjukvard]},
            "jobstream": 971,
        },
        {
            "params": {"date": "2020-01-01T00:00:01", "occupation-concept-id": [group_grundutbildade_sjukskoterskor]},
            "jobstream": 396,
        },
        {
            "params": {"date": "2020-01-01T00:00:01", "occupation-concept-id": [sjukskoterska__grundutbildad]},
            "jobstream": 392,
        },
    ],
)
def test_filter_narrowing_down_occupations_within_same_field_legacy(test_case, endpoint):
    """
    Number of ads should be fewer when searching for field, group and occupation
    Using named concept ids to make it easier to understand relations
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": "heGV_uHh_o8W",
                "location-concept-id": ["8deT_FRF_2SP", "N1wJ_Cuu_7Cs"],
            },
            "jobstream": 2,
        },
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": "heGV_uHh_o8W",
                "location-concept-id": ["zdoY_6u5_Krt", "oLT3_Q9p_3nn"],
            },
            "jobstream": 4,
        },
        pytest.param(
            {
                "params": {
                    "updated-after": "2020-01-01T00:00:01",
                    "occupation-concept-id": "XeBP_nMe_pXx",
                    "location-concept-id": ["u8qF_qpq_R5W", "i46j_HmG_v64"],
                },
                "jobstream": 19,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": "9puE_nYg_crq",
                "location-concept-id": ["Q78b_oCw_Yq2", "AvNB_uwa_6n6"],
            },
            "jobstream": 10,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": "kJeN_wmw_9wX",
                "location-concept-id": ["Q78b_oCw_Yq2", "CifL_Rzy_Mku"],
            },
            "jobstream": 13,
        },
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": "DJh5_yyF_hEM",
                "location-concept-id": ["zdoY_6u5_Krt", "Q78b_oCw_Yq2"],
            },
            "jobstream": 61,
        },
    ],
)
def test_filter_with_date_and_occupation_and_two_locations(test_case, endpoint):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": "heGV_uHh_o8W",
                "location-concept-id": ["8deT_FRF_2SP", "N1wJ_Cuu_7Cs"],
            },
            "jobstream": 2,
        },
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": "heGV_uHh_o8W",
                "location-concept-id": ["zdoY_6u5_Krt", "oLT3_Q9p_3nn"],
            },
            "jobstream": 4,
        },
        pytest.param(
            {
                "params": {
                    "date": "2020-01-01T00:00:01",
                    "occupation-concept-id": "XeBP_nMe_pXx",
                    "location-concept-id": ["u8qF_qpq_R5W", "i46j_HmG_v64"],
                },
                "jobstream": 19,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": "9puE_nYg_crq",
                "location-concept-id": ["Q78b_oCw_Yq2", "AvNB_uwa_6n6"],
            },
            "jobstream": 10,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": "kJeN_wmw_9wX",
                "location-concept-id": ["Q78b_oCw_Yq2", "CifL_Rzy_Mku"],
            },
            "jobstream": 13,
        },
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": "DJh5_yyF_hEM",
                "location-concept-id": ["zdoY_6u5_Krt", "Q78b_oCw_Yq2"],
            },
            "jobstream": 61,
        },
    ],
)
def test_filter_with_date_and_occupation_and_two_locations_legacy(test_case, endpoint):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": ["rz2m_96d_vyF", "heGV_uHh_o8W"],
                "location-concept-id": ["PVZL_BQT_XtL", "N1wJ_Cuu_7Cs"],
            },
            "jobstream": 2,
        },
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": ["rz2m_96d_vyF", "bXNH_MNX_dUR"],
                "location-concept-id": ["Jkyb_5MQ_7pB", "N1wJ_Cuu_7Cs"],
            },
            "jobstream": 3,
        },
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": ["DJh5_yyF_hEM", "heGV_uHh_o8W"],
                "location-concept-id": ["Jkyb_5MQ_7pB", "CifL_Rzy_Mku"],
            },
            "jobstream": 114,
        },
        pytest.param(
            {
                "params": {
                    "updated-after": "2020-01-01T00:00:01",
                    "occupation-concept-id": ["bXNH_MNX_dUR", "TMsM_oNw_j6z"],
                    "location-concept-id": ["oDpK_oZ2_WYt", "bm2x_1mr_Qhx"],
                },
                "jobstream": 18,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "updated-after": "2020-01-01T00:00:01",
                "occupation-concept-id": ["CQBS_ZSZ_ViP", "TMsM_oNw_j6z"],
                "location-concept-id": ["u8qF_qpq_R5W", "i46j_HmG_v64"],
            },
            "jobstream": 17,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["aRFB_gjn_92K", "9puE_nYg_crq"],
                "location-concept-id": ["Q78b_oCw_Yq2", "AvNB_uwa_6n6"],
            },
            "jobstream": 10,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["ek9W_CmD_y2M", "kJeN_wmw_9wX"],
                "location-concept-id": ["Q78b_oCw_Yq2", "CifL_Rzy_Mku"],
            },
            "jobstream": 23,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["Z8ci_bBE_tmx", "j7Cq_ZJe_GkT"],
                "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
            },
            "jobstream": 4,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["Z8ci_bBE_tmx", "NYW6_mP6_vwf"],
                "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
            },
            "jobstream": 5,
        },
        {
            "params": {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": ["NYW6_mP6_vwf", "qSXj_aXc_EGp"],
                "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
            },
            "jobstream": 5,
        },
    ],
)
def test_filter_with_date_and_two_occupations_and_two_locations(test_case, endpoint):
    """
    should return results based on date AND (occupation 1 OR occupation 2) AND (location_1 OR location_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": ["rz2m_96d_vyF", "heGV_uHh_o8W"],
                "location-concept-id": ["PVZL_BQT_XtL", "N1wJ_Cuu_7Cs"],
            },
            "jobstream": 2,
        },
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": ["rz2m_96d_vyF", "bXNH_MNX_dUR"],
                "location-concept-id": ["Jkyb_5MQ_7pB", "N1wJ_Cuu_7Cs"],
            },
            "jobstream": 3,
        },
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": ["DJh5_yyF_hEM", "heGV_uHh_o8W"],
                "location-concept-id": ["Jkyb_5MQ_7pB", "CifL_Rzy_Mku"],
            },
            "jobstream": 114,
        },
        pytest.param(
            {
                "params": {
                    "date": "2020-01-01T00:00:01",
                    "occupation-concept-id": ["bXNH_MNX_dUR", "TMsM_oNw_j6z"],
                    "location-concept-id": ["oDpK_oZ2_WYt", "bm2x_1mr_Qhx"],
                },
                "jobstream": 18,
            },
            marks=pytest.mark.smoke,
        ),
        {
            "params": {
                "date": "2020-01-01T00:00:01",
                "occupation-concept-id": ["CQBS_ZSZ_ViP", "TMsM_oNw_j6z"],
                "location-concept-id": ["u8qF_qpq_R5W", "i46j_HmG_v64"],
            },
            "jobstream": 17,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["aRFB_gjn_92K", "9puE_nYg_crq"],
                "location-concept-id": ["Q78b_oCw_Yq2", "AvNB_uwa_6n6"],
            },
            "jobstream": 10,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["ek9W_CmD_y2M", "kJeN_wmw_9wX"],
                "location-concept-id": ["Q78b_oCw_Yq2", "CifL_Rzy_Mku"],
            },
            "jobstream": 23,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["Z8ci_bBE_tmx", "j7Cq_ZJe_GkT"],
                "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
            },
            "jobstream": 4,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["Z8ci_bBE_tmx", "NYW6_mP6_vwf"],
                "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
            },
            "jobstream": 5,
        },
        {
            "params": {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": ["NYW6_mP6_vwf", "qSXj_aXc_EGp"],
                "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
            },
            "jobstream": 5,
        },
    ],
)
def test_filter_with_date_and_two_occupations_and_two_locations_legacy(test_case, endpoint):
    """
    should return results based on date AND (occupation 1 OR occupation 2) AND (location_1 OR location_2)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "2020-01-01T00:00:01"}, "jobstream": 5029},
        {"params": {"updated-after": "2021-01-01T00:00:01"}, "jobstream": 4904},
        {"params": {"updated-after": "2021-03-20T12:00:00"}, "jobstream": 1045},
        {"params": {"updated-after": "2021-03-22T12:00:00"}, "jobstream": 846},
        {"params": {"updated-after": "2021-03-23T12:00:00"}, "jobstream": 549},
        pytest.param({"params": {"updated-after": "2021-03-24T12:00:00"}, "jobstream": 192}, marks=pytest.mark.smoke),
        {"params": {"updated-after": "2021-03-25T12:30:40"}, "jobstream": 6},
    ],
)
def test_filter_only_on_date(test_case, endpoint):
    """
    Test basic stream with filtering on date (update after this date)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "2020-01-01T00:00:01"}, "jobstream": 5029},
        {"params": {"date": "2021-01-01T00:00:01"}, "jobstream": 4904},
        {"params": {"date": "2021-03-20T12:00:00"}, "jobstream": 1045},
        {"params": {"date": "2021-03-22T12:00:00"}, "jobstream": 846},
        {"params": {"date": "2021-03-23T12:00:00"}, "jobstream": 549},
        pytest.param({"params": {"date": "2021-03-24T12:00:00"}, "jobstream": 192}, marks=pytest.mark.smoke),
        {"params": {"date": "2021-03-25T12:30:40"}, "jobstream": 6},
    ],
)
def test_filter_only_on_date_legacy(test_case, endpoint):
    """
    Test basic stream with filtering on date (update after this date)
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", STREAM_ENDPOINT)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "2021-03-22T00:00:00"}, "expected_ad_ids": [24484317]},
        {"params": {"updated-after": "2021-03-22T00:00:00"}, "expected_ad_ids": [24484317, 24612305, 24612388, 24613432, 24613487, 24613952, 24623857, 24623859, 24626802, 24628869]},
        {"params": {"updated-after": "2021-03-23T01:30:00"}, "expected_ad_ids": [24645311]},
        pytest.param({"params": {"updated-after": "2021-03-23T01:30:00"}, "expected_ad_ids": [24645311, 24629043, 24640957, 24641095, 24641555, 24641643, 24641698, 24644298]}, marks=pytest.mark.smoke),
    ],
)
def test_filter_on_publication_date_and_older_timestamp(test_case, endpoint):
    expected_ad_ids = test_case["expected_ad_ids"]

    ads = get_ads_from_endpoint(test_case, endpoint)
    log.info(f'Number of hits: {len(ads)}')

    ad_ids = set([int(ad["id"]) for ad in ads])

    for expected_ad_id in expected_ad_ids:
        assert expected_ad_id in ad_ids, f"Expected ad id: {expected_ad_id} not found in returned ad ids: {ad_ids}"

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_ENDPOINT)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "2021-03-22T00:00:00"}, "expected_ad_ids": [24484317]},
        {"params": {"date": "2021-03-22T00:00:00"}, "expected_ad_ids": [24484317, 24612305, 24612388, 24613432, 24613487, 24613952, 24623857, 24623859, 24626802, 24628869]},
        {"params": {"date": "2021-03-23T01:30:00"}, "expected_ad_ids": [24645311]},
        pytest.param({"params": {"date": "2021-03-23T01:30:00"}, "expected_ad_ids": [24645311, 24629043, 24640957, 24641095, 24641555, 24641643, 24641698, 24644298]}, marks=pytest.mark.smoke),
    ],
)
def test_filter_on_publication_date_and_older_timestamp_legacy(test_case, endpoint):
    expected_ad_ids = test_case["expected_ad_ids"]

    ads = get_ads_from_endpoint(test_case, endpoint)
    log.info(f'Number of hits: {len(ads)}')

    ad_ids = set([int(ad["id"]) for ad in ads])

    for expected_ad_id in expected_ad_ids:
        assert expected_ad_id in ad_ids, f"Expected ad id: {expected_ad_id} not found in returned ad ids: {ad_ids}"
        
@pytest.mark.parametrize("endpoint", STREAM_ENDPOINT)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"updated-after": "2021-03-22T00:00:00"}},
        {"params": {"updated-after": "2021-03-22T00:00:10"}},
        pytest.param({"params": {"updated-after": "2021-03-22T00:59:00"}}, marks=pytest.mark.smoke),
        {"params": {"updated-after": "2021-03-24T00:00:00"}},
        {"params": {"updated-after": "2021-03-24T00:59:00"}},
        {"params": {"updated-after": "2021-03-24T12:59:00"}},
        {"params": {"updated-after": "2021-03-24T23:59:00"}},
    ],
)
def test_filter_on_date_publication_hour(test_case, endpoint):
    # Verify that returned ads have the right gte hour for publication_date
    # when result is because of corresponding publication_date even though the timestamp is older
    params = test_case["params"]
    input_date = params["updated-after"]

    ads = get_ads_from_endpoint(test_case, endpoint)
    log.info(f'Number of hits: {len(ads)}')
    ads_timestamp_lte_publication_date_count = 0

    # Set timestamp to Stockholm, otherwise the test will fail when running in Gitlab.
    tz_stockholm = timezone('Europe/Stockholm')

    for ad in ads:
        publication_date = ad["publication_date"]
        timestamp = float(ad["timestamp"])/1000

        publication_date_dt = datetime.strptime(publication_date, constants.DATE_FORMAT)
        publication_date_dt = publication_date_dt.replace(tzinfo=tz_stockholm)
        timestamp_dt = datetime.fromtimestamp(timestamp, tz=tz_stockholm)

        timestamp_str = timestamp_dt.strftime(constants.DATE_FORMAT)

        if timestamp_dt < publication_date_dt:
            ads_timestamp_lte_publication_date_count += 1
            assert publication_date >= input_date, f"Publication date: {publication_date} is not >= input_date: {input_date} for ad id: {ad['id']}. Ad timestamp: {ad['timestamp']} - Timestamp str: {timestamp_str}"

    log.info(f'Number of ads with timestamp_dt < publication_date_dt: {ads_timestamp_lte_publication_date_count}')

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_ENDPOINT)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "2021-03-22T00:00:00"}},
        {"params": {"date": "2021-03-22T00:00:10"}},
        pytest.param({"params": {"date": "2021-03-22T00:59:00"}}, marks=pytest.mark.smoke),
        {"params": {"date": "2021-03-24T00:00:00"}},
        {"params": {"date": "2021-03-24T00:59:00"}},
        {"params": {"date": "2021-03-24T12:59:00"}},
        {"params": {"date": "2021-03-24T23:59:00"}},
    ],
)
def test_filter_on_date_publication_hour_legacy(test_case, endpoint):
    # Verify that returned ads have the right gte hour for publication_date
    # when result is because of corresponding publication_date even though the timestamp is older
    params = test_case["params"]
    input_date = params["date"]

    ads = get_ads_from_endpoint(test_case, endpoint)
    log.info(f'Number of hits: {len(ads)}')
    ads_timestamp_lte_publication_date_count = 0

    # Set timestamp to Stockholm, otherwise the test will fail when running in Gitlab.
    tz_stockholm = timezone('Europe/Stockholm')

    for ad in ads:
        publication_date = ad["publication_date"]
        timestamp = float(ad["timestamp"])/1000

        publication_date_dt = datetime.strptime(publication_date, constants.DATE_FORMAT)
        publication_date_dt = publication_date_dt.replace(tzinfo=tz_stockholm)
        timestamp_dt = datetime.fromtimestamp(timestamp, tz=tz_stockholm)

        timestamp_str = timestamp_dt.strftime(constants.DATE_FORMAT)

        if timestamp_dt < publication_date_dt:
            ads_timestamp_lte_publication_date_count += 1
            assert publication_date >= input_date, f"Publication date: {publication_date} is not >= input_date: {input_date} for ad id: {ad['id']}. Ad timestamp: {ad['timestamp']} - Timestamp str: {timestamp_str}"

    log.info(f'Number of ads with timestamp_dt < publication_date_dt: {ads_timestamp_lte_publication_date_count}')

@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        # 1 verify that results are the same as when only using a single date
        {"params": {"updated-after": "2020-01-01T00:00:01", "updated-before": current_time_stamp}, "jobstream": 5029},
        {"params": {"updated-after": "2021-03-30T08:29:41", "updated-before": "2021-04-30T00:00:00"}, "jobstream": 6},
        {"params": {"updated-after": "2020-12-14T00:00:00", "updated-before": "2021-03-30T00:00:00"}, "jobstream": 4944},
        {"params": {"updated-after": "2020-12-14T00:00:00", "updated-before": "2020-12-16T00:00:00"}, "jobstream": 7},
        {"params": {"updated-after": "2020-11-25T00:00:00", "updated-before": "2020-11-30T00:00:00"}, "jobstream": 12},
        {"params": {"updated-after": "2020-11-26T00:00:00", "updated-before": "2020-11-30T00:00:00"}, "jobstream": 7},
        pytest.param(
            {"params": {"updated-after": "2020-12-10T00:00:00", "updated-before": "2020-12-15T00:00:00"}, "jobstream": 15},
            marks=pytest.mark.smoke,
        ),
        {"params": {"updated-after": "2020-12-22T00:00:00", "updated-before": "2020-12-23T10:00:00"}, "jobstream": 4},
        {"params": {"updated-after": "2020-01-01T00:00:01", "updated-before": "2021-04-30T10:00:00"}, "jobstream": 5029},
        {"params": {"updated-after": "2020-01-01T00:00:01", "updated-before": "2023-11-09T07:42:20"}, "jobstream": 5029},
    ],
)
def test_filter_on_date_interval(test_case, endpoint):
    """
    Test stream with filtering on date interval.
    """
    run_test_case(test_case, endpoint)

@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"date": "2020-01-01T00:00:01", "updated-before-date": current_time_stamp}, "jobstream": 5029},
        {"params": {"date": "2021-03-30T08:29:41", "updated-before-date": "2021-04-30T00:00:00"}, "jobstream": 6},
        {"params": {"date": "2020-12-14T00:00:00", "updated-before-date": "2021-03-30T00:00:00"}, "jobstream": 4944},
        {"params": {"date": "2020-12-14T00:00:00", "updated-before-date": "2020-12-16T00:00:00"}, "jobstream": 7},
        {"params": {"date": "2020-11-25T00:00:00", "updated-before-date": "2020-11-30T00:00:00"}, "jobstream": 12},
        {"params": {"date": "2020-11-26T00:00:00", "updated-before-date": "2020-11-30T00:00:00"}, "jobstream": 7},
        pytest.param(
            {"params": {"date": "2020-12-10T00:00:00", "updated-before-date": "2020-12-15T00:00:00"}, "jobstream": 15},
            marks=pytest.mark.smoke,
        ),
        {"params": {"date": "2020-12-22T00:00:00", "updated-before-date": "2020-12-23T10:00:00"}, "jobstream": 4},
        {"params": {"date": "2020-01-01T00:00:01", "updated-before-date": "2021-04-30T10:00:00"}, "jobstream": 5029},
        {"params": {"date": "2020-01-01T00:00:01", "updated-before-date": "2023-11-09T07:42:20"}, "jobstream": 5029},
        {"params": {"date": "2020-01-01T00:00:01"}, "jobstream": 5029},
    ],
)
def test_filter_on_date_interval_legacy(test_case, endpoint):
    """
    Test stream with filtering on date interval.
    """
    run_test_case(test_case, endpoint)    


@pytest.mark.smoke
@pytest.mark.parametrize("endpoint", STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "updated-after": "2020-01-25T00:00:00",
                "updated-before": "2021-11-30T00:00:00",
                "occupation-concept-id": "eU1q_zvL_9Rf",
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 31,
        }
    ],
)
def test_all_params(test_case, endpoint):
    run_test_case(test_case, endpoint)

@pytest.mark.smoke
@pytest.mark.parametrize("endpoint", LEGACY_STREAM_AND_FEED_ENDPOINTS)
@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {
                "date": "2020-01-25T00:00:00",
                "updated-before-date": "2021-11-30T00:00:00",
                "occupation-concept-id": "eU1q_zvL_9Rf",
                "location-concept-id": "CifL_Rzy_Mku",
            },
            "jobstream": 31,
        }
    ],
)
def test_all_params_legacy(test_case, endpoint):
    run_test_case(test_case, endpoint)
