# -*- coding: utf-8 -*-
import pytest
from jobstream.constants import FEED_LEGACY, FEED_V2

from tests.test_resources.jobstream_helper import get_feed, validate_feed_header, validate_feed_single_entry

FEED_ENDPOINT = [FEED_V2]
LEGACY_FEED_ENDPOINT = [FEED_LEGACY]
"""
Testing feed-specific attributes

"""


@pytest.mark.parametrize("endpoint", FEED_ENDPOINT)
@pytest.mark.parametrize(
    "params",
    [
        {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "bUNp_6HW_Vbn"},
        {"updated-after": "1971-01-01T00:00:01", "occupation-concept-id": "DJh5_yyF_hEM", "location-concept-id": "Q78b_oCw_Yq2"},
        pytest.param(
            {
                "updated-after": "2020-12-01T00:00:01",
                "occupation-concept-id": "bH5L_uXD_ZAX",
                "location-concept-id": "Q78b_oCw_Yq2",
            },
            marks=pytest.mark.smoke,
        ),
        {
            "updated-after": "1971-01-01T00:00:01",
            "occupation-concept-id": "XeBP_nMe_pXx",
            "location-concept-id": ["oDpK_oZ2_WYt", "wjee_qH2_yb6"],
        },
        {
            "updated-after": "1971-01-01T00:00:01",
            "occupation-concept-id": "XeBP_nMe_pXx",
            "location-concept-id": ["oDpK_oZ2_WYt", "bm2x_1mr_Qhx"],
        },
        {
            "updated-after": "2020-12-01T00:00:01",
            "occupation-concept-id": "j7Cq_ZJe_GkT",
            "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
        },
        {
            "updated-after": "2020-02-01T00:00:01",
            "occupation-concept-id": "qSXj_aXc_EGp",
            "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
        },
        {
            "updated-after": "1971-01-01T00:00:01",
            "occupation-concept-id": ["pAdE_uVY_JrR", "WNVp_RYe_zLX"],
            "location-concept-id": ["oDpK_oZ2_WYt", "wjee_qH2_yb6"],
        },
        {
            "updated-after": "2020-12-01T00:00:01",
            "occupation-concept-id": ["4KhP_FxL_uZ5", "j7Cq_ZJe_GkT"],
            "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
        },
        {
            "updated-after": "1971-01-01T00:00:01",
            "occupation-concept-id": ["xGr7_SGS_U3F", "DJh5_yyF_hEM"],
            "location-concept-id": ["oDpK_oZ2_WYt", "Q78b_oCw_Yq2"],
        },
        {"updated-after": "2020-12-15T00:00:00", "updated-before-date": "2020-12-16T00:00:00"},
        {"updated-after": "2022-05-09T09:47:13", "updated-before-date": "1971-01-01T00:00:01"},
        {
            "updated-after": "2020-12-01T00:00:01",
            "occupation-concept-id": ["bH5L_uXD_ZAX", "qSXj_aXc_EGp"],
            "location-concept-id": "QJgN_Zge_BzJ",
        },
    ],
)
def test_only_header(endpoint, params):
    xml_str = get_feed(endpoint, params)
    validate_feed_header(xml_str)

@pytest.mark.parametrize("endpoint", LEGACY_FEED_ENDPOINT)
@pytest.mark.parametrize(
    "params",
    [
        {"date": "1971-01-01T00:00:01", "location-concept-id": "bUNp_6HW_Vbn"},
        {"date": "1971-01-01T00:00:01", "occupation-concept-id": "DJh5_yyF_hEM", "location-concept-id": "Q78b_oCw_Yq2"},
        pytest.param(
            {
                "date": "2020-12-01T00:00:01",
                "occupation-concept-id": "bH5L_uXD_ZAX",
                "location-concept-id": "Q78b_oCw_Yq2",
            },
            marks=pytest.mark.smoke,
        ),
        {
            "date": "1971-01-01T00:00:01",
            "occupation-concept-id": "XeBP_nMe_pXx",
            "location-concept-id": ["oDpK_oZ2_WYt", "wjee_qH2_yb6"],
        },
        {
            "date": "1971-01-01T00:00:01",
            "occupation-concept-id": "XeBP_nMe_pXx",
            "location-concept-id": ["oDpK_oZ2_WYt", "bm2x_1mr_Qhx"],
        },
        {
            "date": "2020-12-01T00:00:01",
            "occupation-concept-id": "j7Cq_ZJe_GkT",
            "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
        },
        {
            "date": "2020-02-01T00:00:01",
            "occupation-concept-id": "qSXj_aXc_EGp",
            "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
        },
        {
            "date": "1971-01-01T00:00:01",
            "occupation-concept-id": ["pAdE_uVY_JrR", "WNVp_RYe_zLX"],
            "location-concept-id": ["oDpK_oZ2_WYt", "wjee_qH2_yb6"],
        },
        {
            "date": "2020-12-01T00:00:01",
            "occupation-concept-id": ["4KhP_FxL_uZ5", "j7Cq_ZJe_GkT"],
            "location-concept-id": ["Q78b_oCw_Yq2", "QJgN_Zge_BzJ"],
        },
        {
            "date": "1971-01-01T00:00:01",
            "occupation-concept-id": ["xGr7_SGS_U3F", "DJh5_yyF_hEM"],
            "location-concept-id": ["oDpK_oZ2_WYt", "Q78b_oCw_Yq2"],
        },
        {"date": "2020-12-15T00:00:00", "updated-before-date": "2020-12-16T00:00:00"},
        {"date": "2022-05-09T09:47:13", "updated-before-date": "1971-01-01T00:00:01"},
        {
            "date": "2020-12-01T00:00:01",
            "occupation-concept-id": ["bH5L_uXD_ZAX", "qSXj_aXc_EGp"],
            "location-concept-id": "QJgN_Zge_BzJ",
        },
    ],
)
def test_only_header_legacy(endpoint, params):
    xml_str = get_feed(endpoint, params)
    validate_feed_header(xml_str)


@pytest.mark.parametrize("endpoint", FEED_ENDPOINT)
@pytest.mark.parametrize(
    "params",
    [
        {"updated-after": "1971-01-01T00:00:01", "location-concept-id": "u8qF_qpq_R5W"},
        pytest.param({"updated-after": "1971-01-01T00:00:01", "location-concept-id": "Q78b_oCw_Yq2"}, marks=pytest.mark.smoke),
        {"updated-after": "2020-12-01T00:00:01", "location-concept-id": "Q78b_oCw_Yq2"},
    ],
)
def test_single_entry(endpoint, params):
    xml_str = get_feed(endpoint, params)
    validate_feed_header(xml_str)
    validate_feed_single_entry(xml_str)

@pytest.mark.parametrize("endpoint", LEGACY_FEED_ENDPOINT)
@pytest.mark.parametrize(
    "params",
    [
        {"date": "1971-01-01T00:00:01", "location-concept-id": "u8qF_qpq_R5W"},
        pytest.param({"date": "1971-01-01T00:00:01", "location-concept-id": "Q78b_oCw_Yq2"}, marks=pytest.mark.smoke),
        {"date": "2020-12-01T00:00:01", "location-concept-id": "Q78b_oCw_Yq2"},
    ],
)
def test_single_entry_legacy(endpoint, params):
    xml_str = get_feed(endpoint, params)
    validate_feed_header(xml_str)
    validate_feed_single_entry(xml_str)
