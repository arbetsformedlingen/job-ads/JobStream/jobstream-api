# -*- coding: utf-8 -*-
import json

import pytest
import requests
from jobstream.constants import (
    API_VERSION,
    FEED_LEGACY,
    FEED_V2,
    SNAPSHOT_LEGACY,
    SNAPSHOT_V2,
    STREAM_LEGACY,
    STREAM_V2,
)
from jobstream.endpoints import responses_snapshot, responses_stream_feed

from tests.test_resources.test_settings import TEST_URL


@pytest.mark.parametrize(
    "endpoint, responses",
    [
        (FEED_V2, responses_stream_feed),
        (FEED_LEGACY, responses_stream_feed),
        (STREAM_V2, responses_stream_feed),
        (STREAM_LEGACY, responses_stream_feed),
        (STREAM_V2, responses_stream_feed),
        (SNAPSHOT_V2, responses_snapshot),
        (SNAPSHOT_LEGACY, responses_snapshot),
        (SNAPSHOT_V2, responses_snapshot),
    ],
)
def test_swagger(endpoint, responses):
    """
    Test Swagger info
    """
    response = requests.get(f"{TEST_URL}/swagger.json")
    response.raise_for_status()
    response_json = json.loads(response.content)
    assert response_json["info"]["version"] == API_VERSION
    assert response_json["paths"]["/stream"]
    path = f"/{endpoint}"
    assert len(response_json["paths"][path]["get"]["responses"]) == len(responses)
