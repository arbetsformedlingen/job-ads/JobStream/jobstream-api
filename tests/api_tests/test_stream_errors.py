# -*- coding: utf-8 -*-
import pytest
import requests
from jobstream.constants import (
    FEED_LEGACY,
    FEED_V2,
    LOCATION_CONCEPT_ID,
    OCCUPATION_CONCEPT_ID,
    STREAM_LEGACY,
    STREAM_V2,
)

from tests.test_resources.jobstream_helper import get_stream_expect_error, get_stream_with_endpoint
from tests.test_resources.test_settings import DAWN_OF_TIME


@pytest.mark.parametrize("endpoint", [STREAM_V2, FEED_V2])
@pytest.mark.parametrize(
    "wrong_date",
    [
        "2020-13-25T00:00:00",
        "2020-034-25T00:00:00",
        "20-00-25T00:00:00",
        "0001-00-01",
        "T11:28:00",
        "2022-05-22T12:20:52 fm",
    ],
)
def test_wrong_date_format(wrong_date, endpoint):
    get_stream_expect_error(
        params={"updated-after": wrong_date}, expected_http_code=requests.codes.bad_request, endpoint=endpoint
    )

@pytest.mark.parametrize("endpoint", [STREAM_LEGACY, FEED_LEGACY])
@pytest.mark.parametrize(
    "wrong_date",
    [
        "2020-13-25T00:00:00",
        "2020-034-25T00:00:00",
        "20-00-25T00:00:00",
        "0001-00-01",
        "T11:28:00",
        "2022-05-22T12:20:52 fm",
    ],
)
def test_wrong_date_format_legacy(wrong_date, endpoint):
    get_stream_expect_error(
        params={"date": wrong_date}, expected_http_code=requests.codes.bad_request, endpoint=endpoint
    )


@pytest.mark.parametrize("endpoint", [STREAM_V2, STREAM_LEGACY, FEED_V2, FEED_LEGACY])
@pytest.mark.parametrize(
    "taxonomy_type, value", [(OCCUPATION_CONCEPT_ID, "fLps_qRU_1CC"), (LOCATION_CONCEPT_ID, "AvNB_uwa_6n6")]
)
def test_filter_without_date_expect_bad_request_response(taxonomy_type, value, endpoint):
    """
    test that a 'bad request' response (http 400) is returned when doing a request without date parameter
    """
    get_stream_expect_error(
        params={taxonomy_type: value}, expected_http_code=requests.codes.bad_request, endpoint=endpoint
    )


@pytest.mark.parametrize("endpoint", [STREAM_V2])
@pytest.mark.parametrize("work, expected_number_of_hits", [("DJh5_yyF_hEM", 239), ("DJh5_yyF_hEM".lower(), 0)])
def test_filter_with_lowercase_concept_id(work, expected_number_of_hits, endpoint):
    """
    compare correct concept_id with a lower case version
    """
    params = {"updated-after": DAWN_OF_TIME, OCCUPATION_CONCEPT_ID: work}
    hits = get_stream_with_endpoint(params=params, endpoint=endpoint)
    assert len(hits) == expected_number_of_hits, f"Got {len(hits)} but expected {expected_number_of_hits}"

@pytest.mark.parametrize("endpoint", [STREAM_LEGACY])
@pytest.mark.parametrize("work, expected_number_of_hits", [("DJh5_yyF_hEM", 239), ("DJh5_yyF_hEM".lower(), 0)])
def test_filter_with_lowercase_concept_id_legacy(work, expected_number_of_hits, endpoint):
    """
    compare correct concept_id with a lower case version
    """
    params = {"date": DAWN_OF_TIME, OCCUPATION_CONCEPT_ID: work}
    hits = get_stream_with_endpoint(params=params, endpoint=endpoint)
    assert len(hits) == expected_number_of_hits, f"Got {len(hits)} but expected {expected_number_of_hits}"


# Todo: test feed endpoints


@pytest.mark.parametrize("endpoint", [STREAM_V2, STREAM_LEGACY, FEED_V2, FEED_LEGACY])
@pytest.mark.parametrize("abroad", [True, False])
def test_work_abroad(abroad, endpoint):
    """
    Check that param 'arbete-utomlands' returns http 400 BAD REQUEST for stream
    """
    get_stream_expect_error(
        {"arbete-utomlands": abroad}, expected_http_code=requests.codes.bad_request, endpoint=endpoint
    )


@pytest.mark.parametrize("endpoint", [STREAM_V2])
def test_response_format(endpoint):
    hits = get_stream_with_endpoint(params={"updated-after": "2021-03-30T00:00:00"}, endpoint=endpoint)

    for hit in hits:
        for x in ["occupation", "occupation_group", "occupation_field"]:
            assert isinstance(hit[x], dict), f"ERROR: expected a dict, but got {type(hit[x])}"

@pytest.mark.parametrize("endpoint", [STREAM_LEGACY])
def test_response_format_legacy(endpoint):
    hits = get_stream_with_endpoint(params={"date": "2021-03-30T00:00:00"}, endpoint=endpoint)

    for hit in hits:
        for x in ["occupation", "occupation_group", "occupation_field"]:
            assert isinstance(hit[x], dict), f"ERROR: expected a dict, but got {type(hit[x])}"
