# -*- coding: utf-8 -*-
import pytest
from jobstream.constants import (
    SNAPSHOT_LEGACY,
    SNAPSHOT_V2,
    STREAM_LEGACY,
    STREAM_V2,
)

from tests.test_resources.jobstream_helper import get_snapshot, get_stream_with_endpoint
from tests.test_resources.test_settings import DAWN_OF_TIME


@pytest.fixture(scope="session")
def jobstream_fixture(request):
    """
    get ads from the stream or snapshot endpoints based on parameter
    usage:

    from stream.endpoint_names import STREAM_LEGACY, STREAM_v2_, STREAM_V2

    @pytest.mark.parametrize("jobstream_fixture", [STREAM_v2, SNAPSHOT_V2], indirect=True)
    def test(jobstream_fixture):
        for ad in jobstream_fixture:
            pass

    """
    endpoint = request.param
    if endpoint in [SNAPSHOT_V2, SNAPSHOT_LEGACY]:
        return get_snapshot(endpoint)
    elif request.param in [STREAM_V2]:
        params = {"updated-after": DAWN_OF_TIME}
        return get_stream_with_endpoint(endpoint, params)
    elif request.param in [STREAM_LEGACY]:
        params = {"date": DAWN_OF_TIME}
        return get_stream_with_endpoint(endpoint, params)
    else:
        raise ValueError(f"No such endpoint: {endpoint}")
