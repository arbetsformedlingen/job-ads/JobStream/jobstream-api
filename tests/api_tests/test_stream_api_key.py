# -*- coding: utf-8 -*-
import pytest
from jobstream.constants import OCCUPATION_CONCEPT_ID, SNAPSHOT_LEGACY, STREAM_LEGACY

from tests.test_resources.jobstream_helper import get_snapshot_with_headers, get_stream_with_headers

# TODO: do we need it for streaming? This was test to check that nothing happened after we removed the api-key code


@pytest.mark.parametrize("endpoint", [STREAM_LEGACY])
@pytest.mark.smoke
def test_use_api_key_stream(endpoint):
    """
    Check that nothing breaks when using api-key in header
    """
    test_headers = {"api-key": "test_api_key", "accept": "application/json"}
    params = {"date": "2020-12-15T00:00:01", OCCUPATION_CONCEPT_ID: "YfCD_kUE_kck"}
    get_stream_with_headers(expected_number=8, params=params, headers=test_headers, endpoint=endpoint)


def test_use_api_key_snapshot():
    """
    Check that nothing breaks when using api-key in header
    """
    test_headers = {"api-key": "test_api_key", "accept": "application/json"}
    get_snapshot_with_headers(endpoint=SNAPSHOT_LEGACY, headers=test_headers)
