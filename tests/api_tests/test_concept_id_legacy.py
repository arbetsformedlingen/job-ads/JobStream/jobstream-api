# -*- coding: utf-8 -*-
import pytest
from jobstream.constants import (
    SNAPSHOT_LEGACY,
    SNAPSHOT_V2,
    STREAM_LEGACY,
    STREAM_V2,
)

from tests.test_resources.concept_ids.occupation_concept_ids_and_legacy_ids import (
    occupation_field_pairs,
    occupation_group_pairs,
    occupation_pairs,
)


@pytest.mark.parametrize(
    "jobstream_fixture",
    [STREAM_V2, STREAM_LEGACY, SNAPSHOT_V2, SNAPSHOT_LEGACY, SNAPSHOT_V2],
    indirect=True,
)
def test_occupation_concept_ids_legacy_ids_pair(jobstream_fixture):
    """
    Test that concept ids and legacy taxonomy ids are as expected
    """
    for ad in jobstream_fixture:
        for concept_id, legacy_id in occupation_pairs:
            if concept_id == ad["occupation"]["concept_id"]:
                assert legacy_id == ad["occupation"]["legacy_ams_taxonomy_id"]


@pytest.mark.parametrize(
    "jobstream_fixture",
    [STREAM_V2, STREAM_LEGACY, SNAPSHOT_V2, SNAPSHOT_LEGACY, SNAPSHOT_V2],
    indirect=True,
)
def test_occupation_group_concept_ids_legacy_ids_pair(jobstream_fixture):
    """
    Test that concept ids and legacy taxonomy ids are as expected
    """
    for ad in jobstream_fixture:
        for concept_id, legacy_id in occupation_group_pairs:
            if concept_id == ad["occupation_group"]["concept_id"]:
                assert legacy_id == ad["occupation_group"]["legacy_ams_taxonomy_id"]


@pytest.mark.parametrize(
    "jobstream_fixture",
    [STREAM_V2, STREAM_LEGACY, STREAM_V2, SNAPSHOT_V2, SNAPSHOT_LEGACY, SNAPSHOT_V2],
    indirect=True,
)
def test_occupation_field_concept_ids_legacy_ids_pair(jobstream_fixture):
    """
    Test that concept ids and legacy taxonomy ids are as expected
    """
    for ad in jobstream_fixture:
        for concept_id, legacy_id in occupation_field_pairs:
            if concept_id == ad["occupation_field"]["concept_id"]:
                assert legacy_id == ad["occupation_field"]["legacy_ams_taxonomy_id"]
