# -*- coding: utf-8 -*-
import json
import pathlib

import pytest
from jobstream.load_ads.common import formatter


@pytest.fixture()
def opensearch_data():
    file_path = pathlib.Path(__file__).parent.resolve() / "test_resources" / "ad_from_opensearch.json"
    with open(file_path) as f:
        test_data = json.load(f)
    yield test_data


def test_full_ad(opensearch_data):
    formatted_ad = json.loads(formatter(opensearch_data))

    assert formatted_ad.get("id", None) is not None
    assert formatted_ad.get("removed", None) is not None
    assert formatted_ad.get("removed", None) is False


def test_removed_ad(opensearch_data):
    opensearch_data["_source"]["removed"] = True
    removed_ad = json.loads(formatter(opensearch_data))
    assert removed_ad.get("id", None) is not None
    assert removed_ad.get("removed", None) is not None
    assert removed_ad.get("removed", None) is True

    assert isinstance(removed_ad.get("municipality", None), str)
    assert isinstance(removed_ad.get("region", None), str)
    assert isinstance(removed_ad.get("country", None), str)
    assert isinstance(removed_ad.get("occupation", None), dict)
