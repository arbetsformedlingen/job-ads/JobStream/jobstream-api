# -*- coding: utf-8 -*-
import json
from pathlib import Path

from jobstream.constants import API_VERSION

expected_feed_response = bytes(
    f"""<?xml version='1.0' encoding='UTF-8'?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <id>https://jobstream.api.jobtechdev.se/feed</id>
  <title>JobStream Feed</title>
  <updated>2023-05-26T09:30:01.151000+00:00</updated>
  <link href="https://jobstream.api.jobtechdev.se" rel="self"/>
  <generator uri="https://jobstream.api.jobtechdev.se" version="{API_VERSION}">JobTech</generator>
  <subtitle>Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen</subtitle>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27636014</id>
    <title>Arbetsterapeut sökes till Västerås!</title>
    <updated>2023-05-23T11:10:31.852000+00:00</updated>
    <content type="html">Vill du hitta en hållbar balans i livet mellan ditt arbetsliv och fritid med flexibilitet och trygghet? Nu söker vi dig som arbetsterapeut som vill vara en del av Dedicare, Nordens största rekryterings-och bemanningsföretag inom vård, life science och socialt arbete. Vi sätter dina drivkrafter i fokus och ger dig chansen att hitta jobbet som passar just dig. Med uppdragsgivare över hela Sverige kan vi erbjuda dig som arbetsterapeut viktiga och spännande uppdrag där du får möjlighet att bidra och utveckla din kompetens. Vi finns med dig längs hela uppdraget och ser fram emot ett långsiktigt samarbete tillsammans med dig. Om uppdraget:...</content>
    <published>2023-05-23T11:10:31+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27578237</id>
    <title>Arbetsterapeut Handteam Falu lasarett</title>
    <updated>2023-05-11T07:46:38.998000+00:00</updated>
    <content type="html">Hos oss i Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Arbetsterapiverksamheten finns samlad i en gemensam enhet med ca 65 medarbetare, fördelat på tre av länets lasarett. Arbetsterapi Akutrehab, med ca 17 arbetsterapeuter, träffar patienter på Falu lasaretts alla akuta kliniker i både i sluten- och öppenvård. Exempelvis handteam, stroke, allmän medicin och ortopedi, palliativt team, barn, reuma och kirurgi. Arbetsterapeut Handteam Falu lasarett...</content>
    <published>2023-05-11T07:46:38+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27568606</id>
    <title>Arbetsterapeut, Länsvuxenpsykiatrin Falun</title>
    <updated>2023-05-09T14:49:09.141000+00:00</updated>
    <content type="html">Hos oss i Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hållbart Dalarna med utvecklingskraft i länets alla delar. Länsvuxenpsykiatriska kliniken är regionens verktyg för akut psykiatrisk slutenvård och specialiserad psykiatrisk öppenvård. Verksamheten i Falun består av allmänpsykiatrisk vårdavdelning för patienter med psykiska störningar och somatisk samsjuklighet, ECT behandling samt vårdavdelning för patienter med beroendetillstånd med tillhörande akutmottagning. Behandlingsteamet servar Länsvuxenpsykiatrins vårdavdelningar med konsultativa insatser utifrån våra olika yrkeskompetenser. I behandlingsteamet arbetar kurator, psykolog och arbetsterapeut. Vi har ett intressant och varierat arbete där vi möter människor i olika åldrar och skiftande livssituationer. Behandlingsteamets medarbetare samarbetar på olika sätt för att ge våra patienter en god vård. Arbetsterapeut, Länsvuxenpsykiatrin Falun...</content>
    <published>2023-05-09T14:49:09+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27553402</id>
    <title>Qura Care söker arbetsterapeuter för uppdrag, minst 60 000 kr i lön</title>
    <updated>2023-05-05T16:15:01.598000+00:00</updated>
    <content type="html">Qura Care söker nu arbetsterapeuter för högavlönade uppdrag med hög flexibilitet inom kommun och primärvård. Det finns både kortare och längre uppdrag vilket gör att du kan välja ett upplägg som passar just dig. Vi erbjuder dig:...</content>
    <published>2023-05-05T16:15:01+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27537024</id>
    <title>Arbetsterapeut till Primärvårdens arbetsterapeuter</title>
    <updated>2023-05-03T14:48:53.420000+00:00</updated>
    <content type="html">En av våra erfarna medarbetare går nu i pension och vi behöver utöka vårt lag av Arbetsterapeuter. Vi behöver dig med ett genuint engagemang och intresse för arbetsterapeutens arbetsfält. Här arbetar du på en mindre enhet tillsammans med andra arbetsterapeuter, där ni tillsammans har uppdraget att serva länets mottagningar med er kompetens. Om du söker ett omväxlande arbete och vill ingå i ett team med andra arbetsterapeuter, då kanske du är vår nya kollega? Välkommen till en arbetsplats där du är mitt i livet och samhället. Arbetsuppgifter Som arbetsterapeut inom Primärvården kommer du att få en omväxlande och självständig arbetsdag. Du får eget ansvar att strukturera ditt arbete och lägger upp en hållbar planering. Dina huvudsakliga arbetsuppgifter består av handrehabilitering, minnesutredningar och hjälpmedelsutprovning. Tillsammans med andra yrkeskategorier arbetar du i team med multimodal rehabilitering, vardagsrevidering och artrosskola. Verksamheten är under utveckling och vi ser gärna att du trivs i förändringsarbete. Arbetsplatsen...</content>
    <published>2023-05-03T14:48:53+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27528772</id>
    <title>Arbetsterapeut till Habiliteringscentrum</title>
    <updated>2023-05-02T22:55:31.964000+00:00</updated>
    <content type="html">Vill du arbeta som arbetsterapeut och genom stöd, råd och behandling skapa möjligheter för våra patientgrupper att leva ett bra liv? Ta då chansen och sök ett intressant och stimulerande arbete hos oss! Välkommen till en arbetsplats där du är mitt i livet och samhället. Och gör skillnad varje dag. På riktigt. Arbetsuppgifter Vi söker två nya kollegor till Enheten för rörelsenedsättning på Habiliteringscentrum! Enheten för rörelsenedsättning är uppdelade i två team, barn- ungdomteam och vuxenteam och vi söker nu en arbetsterapeut i vardera team. I vuxenteamet arbetar 3 arbetsterapeuter och i barn- ungdomsteamet 4 arbetsterapeuter, vilket ger goda möjligheter till yrkesutveckling, samverkan och kollegialt stöd. Som arbetsterapeut i teamet för rörelsenedsättning kommer du att ge insatser till barn, ungdomar och vuxna med medfödda, omfattande och bestående funktionsnedsättningar, där rörelsenedsättningen är den gemensamma nämnaren. Insatser ges även utifrån andra funktionsnedsättningar vilket gör arbetet som arbetsterapeut både varierande och stimulerande. Arbetet innefattar att hjälpa patienten till en fungerande vardag gällande struktur, hjälpmedel och strategier. Insatserna innebär arbetsterapeutiskt behandling både individuellt och i grupp. Du kommer även ge stöd till familjen, närstående samt olika personalgrupper genom information, utbildning, handledning och konsultation. Patientbesök kan ske både på mottagning, i hemmiljö och i andra verksamheter. Arbetet sker i ett nära och gott samarbete med övriga kollegor i teamet, som består av arbetsterapeuter, fysioterapeuter/sjukgymnaster, handarbetsterapeut, kurator, dietist, specialpedagog, psykolog och logoped. Tillsammans utarbetar ni förslag till åtgärder för att underlätta barnets, ungdomens eller den vuxnes levnadsförhållande. Arbetet innebär också samverkan med vårdgrannar och olika myndigheter t.ex. Hjälpmedelscentrum, barnkliniken, psykiatri och kommunala verksamheter. Journalskrivning och förskrivning av hjälpmedel är uppgifter som ingår i det dagliga arbetet. Nyfiken? Maila gärna oss om du vill boka ett förutsättningslöst samtal så berättar vi mer. Du kan även kontakta oss för ett studiebesök. Du får då möjlighet till en rundtur för att se våra lokaler, träffa några medarbetare och få en inblick i vårt arbete. Om arbetsplatsen...</content>
    <published>2023-05-22T00:00:00+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27642705</id>
    <title>Arbetsterapeut till Närvården Hemdal och Närvården Sala Väsby</title>
    <updated>2023-05-26T09:30:01.151000+00:00</updated>
    <content type="html">Vi är i en spännande utvecklingsfas och söker nu dig som är legitimerad arbetsterapeut. Har du ett engagemang och brett intresse för arbetsterapeutens arbetsområde? Då kan du vara den vi söker! Välkommen till en arbetsplats där du är mitt i livet och samhället. Och gör skillnad varje dag. På riktigt. Du kommer att arbeta nära en minnessköterska och geriatriker med minnesutredningar och uppföljningar. Som arbetsterapeut kommer du även att arbeta med primärbedömningar som första instans och framförallt vad gäller handskador. Arbetet är omväxlande och självständigt, där du tidsmässigt får möjlighet att lägga upp din egen planering. Du arbetar även i team tillsammans med andra yrkeskategorier som exempelvis fysioterapeut, sjuksköterska, läkare, barnavårdscentralen och det psykosociala teamet. Vi har ett tätt samarbete mellan våra vårdcentraler och det ingår i tjänsten att arbeta på båda vårdcentralerna. Vi är en utvecklingsbenägen verksamhet som gillar att tänka modernt och innovativt. Därför är utveckling och förbättringsmöjligheter en naturlig del i det dagliga arbetet för att utveckla vårt arbete och vår verksamhet. Det finns möjlighet att ta ett större ansvar och vi erbjuder kompetensutveckling till dig som medarbetare för att du ska känna dig trygg i din roll. Om arbetsplatsen De 12 regiondrivna vårdcentralerna i Västmanland drivs som en intraprenad med en styrelse och Region Västmanland som ägare. Vi har ett gemensamt namn, Närvården Västmanland, där vi arbetar tillsammans för att ge våra patienter den bästa primärvården i länet. Vi satsar kontinuerligt på utveckling av våra arbetsplatser för att möta framtidens primärvård. Båda vårdcentralerna är inarbetade och relativt stora vårdcentraler, som ligger i nära anslutning till sjukhusområdet i Västerås respektive Sala. Båda verksamheterna omfattar läkar- och distriktssköterskemottagning, mödra- och barnhälsovård, rehabiliteringsenhet med fysioterapeuter och arbetsterapeut samt ett psykosocialt team med kurator, psykolog och sjuksköterska. Vårdcentralerna satsar på att våga och kliver gärna in i nya sätt att tänka för att anpassa vården. Vi vill utvecklas och det finns stora möjligheter att vara med och påverka. Vi erbjuder Dig:...</content>
    <published>2023-05-24T12:18:40+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27631200</id>
    <title>Arbetsterapeut till KommunRehab Lss/Socialpsykiatri</title>
    <updated>2023-05-22T12:05:11.949000+00:00</updated>
    <content type="html">I Falu kommun finns en mängd intressanta och kvalificerade arbetsuppgifter. Vi har ingenjörer, kommunikatörer, lärare, undersköterskor, projektledare och jurister samt 214 andra yrkesbefattningar. Vår vision är Ett större Falun. Vi ska vara enkla att samarbeta med och till nytta för faluborna. Vi gör skillnad och vi har jobb som märks. Vi är dessutom en arbetsgivare som vill ta tillvara våra medarbetares fulla kompetens - det vet vi leder till utveckling både för individen och för organisationen. För oss är det viktigt att ge våra medarbetare ett hållbart arbetsliv, vilket vi kallar ”Balans i livet”. Vi satsar alltså mycket på de mjuka värdena – hur människor mår, blir bemötta och kan växa – eftersom det är vår definition av att vara en riktigt bra arbetsgivare. I Falu kommun eftersträvar vi jämställdhet och vi ser gärna en personalsammansättning som speglar mångfalden i samhället. Vi välkomnar därför sökande som bidrar till detta. Vill du komma till en enhet med stor arbetsglädje och möjlighet till påverkan? Vi söker nu enarbetsterapeut för placering mot LSS och socialpsykiatri. Lockar detta dig kan vi erbjuda ett meningsfullt arbete med varierande och utmanande arbetsuppgifter. Hos oss har du även stora möjligheter att påverka din egen arbetsdag och utvecklas tillsammans med oss. Vår enhets breda kompetens inom rehabilitering är en av våra stora styrkor och det finns stor öppenhet för kompetensutnyttjande internt. KommunRehab är en enhet som växer och utvecklas under sektion Hälso- och sjukvård i Falu kommun. Enheten ansvarar för rehabilitering inom hemsjukvård, vård- och omsorgsboende, LSS och socialpsykiatri samt korttidsvistelse. Hos oss finns arbetsterapeuter, fysioterapeuter, rehabassistenter och hjälpmedelssamordnare. Arbetsuppgifter Hos oss arbetar du med rehabilitering av patienter på primärvårdsnivå i ordinärt boende, på grupp- och serviceboende samt dagligverksamhet och får chansen att nyttja hela din kompetens i ditt arbete. Arbetsuppgifterna är varierande och av problemlösande karaktär med syfte att hitta meningsfulla aktiviteter och öka personens självständighet. Som arbetsterapeut innebär detta bland annat bedömning av aktivitetsförmåga inklusive kognitiva bedömningar, utformning av stöd och strategier samt förskrivning av hjälpmedel. Det innebär även att uppmärksamma behov av vårdplanering/SIP, utbildning och handledning av personal. Du har en varierande, utmanande, lärorik och rolig arbetsmiljö hos patienterna och samarbetar med fysioterapeut, sjuksköterska och en mängd andra aktörer kring patienten. Hos oss på KommunRehab har du kollegor som samarbetar, stöttar med sin erfarenhet och vill nå resultat tillsammans med dig. Du ges möjlighet att utveckla rehabilitering, hälsofrämjande arbete och vidare förstärka befintliga samarbetsformer med aktörer såsom boendestödjare, bistånd, primärvård och slutenvård. Kvalifikationer...</content>
    <published>2023-05-22T12:05:11+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27616516</id>
    <title>Arbetsterapeut till Vuxenhabiliteringen, Falun</title>
    <updated>2023-05-17T15:10:50.798000+00:00</updated>
    <content type="html">Hos oss i Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Vi söker nu en arbetsterapeut till vuxenhabiliteringen, välkommen med din ansökan! Habiliteringen i Region Dalarna är en länsövergripande specialistresurs som finns på fem orter i länet. Vårat uppdrag består i att erbjuda personer med medfödda eller tidigt förvärvade funktionsnedsättningar sammansatta specialistinsatser för att uppnå bästa möjliga funktionsförmåga och en god livssituation. Arbetsterapeut till Vuxenhabiliteringen, Falun...</content>
    <published>2023-05-17T15:10:50+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/24608954</id>
    <title>Arbetsterapeut till Barn – och ungdomspsykiatrin Västerås</title>
    <updated>2021-03-10T11:00:11.780000+00:00</updated>
    <content type="html">Vill du ha ett arbete där du arbetar för att underlätta vardagen och skapa förutsättningar för delaktighet i aktiviteter för barn och ungdomar med olika psykiatriska och neuropsykiatriska funktionsnedsättningar? Då är det kanske dig vi söker till vårt arbetsterapeutteam på Barn – och ungdomspsykiatrin!...</content>
    <published>2021-03-10T11:00:11+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/24498139</id>
    <title>Arbetsterapeuter till kirurgklinikens rehabenhet, sommaren  2021</title>
    <updated>2021-01-25T11:12:52.085000+00:00</updated>
    <content type="html">Vill du vara en del av en betydelsefull verksamhet där du gör skillnad och skapar trygghet för patienten? Kanske är det just du som blir en av våra semestervikarier till sommaren 2021....</content>
    <published>2021-01-25T11:12:52+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/25895627</id>
    <title>Agila söker arbetsterapeuter i Dalarna</title>
    <updated>2022-10-01T00:24:50.915000+00:00</updated>
    <content type="html">Vi på Agila har många lediga uppdrag för arbetsterapeuter. Både på heltid och deltid. Vi söker dig som vill kunna styra mer över din egen tid, detta genom att erbjuda hög flexibilitet samt en god ersättning. Bland våra uppdragsgivare finns kommuner, regioner och privata aktörer från norr till söder i Sverige. VI ERBJUDER DIG...</content>
    <published>2022-04-05T11:46:22+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/26435171</id>
    <title>Forskarförordnanden, Region Dalarna/Uppsala universitet</title>
    <updated>2022-10-03T00:35:10.265000+00:00</updated>
    <content type="html">Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Centrum för Klinisk Forskning (CKF) är ett tvärprofessionellt forskningscentrum inom Region Dalarna och knutet till Uppsala universitet. Centrum för Klinisk Forskning Dalarna utlyser finansiering för forskarförordnanden. ARBETSBESKRIVNING...</content>
    <published>2022-08-22T07:29:52+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/26435110</id>
    <title>Docentförordnande, Region Dalarna/Uppsala universitet</title>
    <updated>2022-10-03T00:35:21.418000+00:00</updated>
    <content type="html">Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Centrum för Klinisk Forskning (CKF) är ett tvärprofessionellt forskningscentrum inom Region Dalarna och knutet till Uppsala universitet. Centrum för Klinisk Forskning Dalarna utlyser finansiering för ett docentförordnande. ARBETSBESKRIVNING...</content>
    <published>2022-08-24T00:00:00+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/26434954</id>
    <title>Postdokförordnanden, Region Dalarna/Uppsala universitet</title>
    <updated>2022-10-03T00:36:11.345000+00:00</updated>
    <content type="html">Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Centrum för Klinisk Forskning (CKF) är ett tvärprofessionellt forskningscentrum inom Region Dalarna och knutet till Uppsala universitet. Centrum för Klinisk Forskning Dalarna utlyser finansiering för postdokförordnanden. ARBETSBESKRIVNING...</content>
    <published>2022-08-22T00:00:00+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/26430015</id>
    <title>Doktorandförordnanden, Region Dalarna/Uppsala universitet</title>
    <updated>2022-10-03T00:35:11.720000+00:00</updated>
    <content type="html">Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Centrum för Klinisk Forskning (CKF) är ett tvärprofessionellt forskningscentrum inom Region Dalarna och knutet till Uppsala universitet. Centrum för Klinisk Forskning Dalarna utlyser finansiering för doktorandförordnanden. ARBETSBESKRIVNING...</content>
    <published>2022-08-22T00:00:00+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/26429755</id>
    <title>Doktorandförordnanden medicinsk etik, Region Dalarna/Uppsala universitet</title>
    <updated>2022-10-03T00:37:31.544000+00:00</updated>
    <content type="html">Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna! Centrum för Klinisk Forskning (CKF) är ett tvärprofessionellt forskningscentrum inom Region Dalarna och knutet till Uppsala universitet. Centrum för Klinisk Forskning Dalarna utlyser finansiering för ett doktorandförordnande inom medicinsk etik med inriktning mot hälso- och sjukvårdens etik (klinisk etik). ARBETSBESKRIVNING...</content>
    <published>2022-08-22T00:00:00+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27490363</id>
    <title>Arbetsterapeut (vikariat), BUP-mottagningen Falun</title>
    <updated>2023-04-24T16:18:04.800000+00:00</updated>
    <content type="html">Hos oss i Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hållbart Dalarna med utvecklingskraft i länets alla delar. Hälso- och sjukvårdsförvaltningen arbetar för att vården ska vara lättillgänglig, av god kvalitet och bygga på respekt för patientens självbestämmande och integritet. Förvaltningen består av fem divisioner: primärvård, psykiatri, kirurgi, medicin och medicinsk service. Vi söker dig som är intresserad av att arbeta i vår spännande verksamhet och delar vår ambition att erbjuda en god vård på lika villkor. BUP Dalarna består av 5 öppenvårdsmottagningar samt länsresurs med nyanmälan och råd, Akutteam, avdelning 68 för dygnsvård, familjeterapienhet och ätstörningsenheten Tornet, samtliga är underställda en gemensam verksamhetschef. Arbetsterapeut (vikariat), BUP-mottagningen Falun...</content>
    <published>2023-04-24T16:18:04+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27461702</id>
    <title>Arbetsterapeut på barn- och ungdomshabiliteringen, Falun</title>
    <updated>2023-04-18T16:53:44.153000+00:00</updated>
    <content type="html">Hos oss i Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hållbart Dalarna med utvecklingskraft i länets alla delar. Habiliteringen är en länsövergripande specialistverksamhet som finns på fem orter i Dalarna. På varje enhet finns barn-och ungdomshabilitering samt vuxenhabilitering bestående av tvärvetenskapliga team med medicinsk, psykologisk, social och pedagogisk kompetens. Habiliteringens uppdrag består i att erbjuda barn, ungdomar och vuxna med medfödda eller tidigt förvärvade funktionsnedsättningar samt till deras närstående sammansatta specialistinsatser för att uppnå bästa möjliga funktionsförmåga och ett fungerande vardagsliv. Vi behöver nu stärka vårt barnteam i Falun med arbetsterapeuter. Som arbetsterapeut får du ett lärorikt och omväxlande arbete - vill du bli en av oss? Arbetsterapeut på barn- och ungdomshabiliteringen, Falun...</content>
    <published>2023-05-01T00:00:00+00:00</published>
    <municipality>Falun</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27456855</id>
    <title>Allegio söker Arbetsterapeut till vår verksamhet i Västerås</title>
    <updated>2023-04-18T09:25:10.569000+00:00</updated>
    <content type="html">Allegio Omsorg är ett familjeomsorgsföretag som vill göra skillnad. Vi erbjuder vård, omsorg och service genom biståndsbeslut eller som RUT-tjänst. Idag är vi på Allegio Omsorg över 500 anställda. Vi är undersköterskor, vårdbiträden, planeringsledare, sjuksköterskor, arbetsterapeuter, fysioterapeuter och lokalvårdare mm. Vi är ingenting utan våra duktiga medarbetare! Kompetens är viktigt – vi har därför en hög utbildningsnivå på våra anställda med särskild inriktning på demens och palliativ omvårdnad. Vi arbetar ständigt med utveckling i våra verksamheter och arbetar kvalitetssäkrande med bl.a. egenkontroller. Namnet Allegio kommer från latinets Legio som betyder ”utvalt manskap” – det är utifrån den devisen vi arbetar: vi vet att vi erbjuder de bästa tjänsterna när vi också har de bästa och mest engagerade medarbetarna! Fler väljer Allegio Omsorg som utförare för sin hemrehabilitering och därför vill vi utöka vårt hemrehabiliteringsteam med en Arbetsterapeut. Följ med oss och utveckla vår verksamhet och hemrehabilitering i riktning mot en god och nära vård! Din personlighet är av stor betydelse. Dina arbetsuppgifter är sedvanliga uppgifter inom uppdraget som arbetsterapeut i hemrehabilitering. I verksamheten finns även fysioterapeut/sjukgymnast, sjuksköterska samt undersköterskor och vårdbiträden. Du kommer att arbeta med övriga medarbetare i teamet runt dina kunder samt samverkan med, för dina kunder, aktuella instanser. En viktig del i arbetet är att tillsammans med övriga i teamet för hemrehabiliteringen utveckla er yrkesroll och samarbetet med övriga professioner. Detta med målet att upprätthålla en så hög patientsäkerhet och kvalitet på våra hälso- och sjukvårdsinsatser som möjligt. Arbetstiden är förlagd till vardagar, dagtid då verksamheten ska bemannas enligt avtal med Västerås stad. Vill du bli en av oss? Välkommen att söka tjänsten som Arbetsterapeut. Till en eventuell intervju ska du ha med dig...</content>
    <published>2023-04-19T00:00:00+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/26532417</id>
    <title>Arbetsterapeut till Ortopedklinikens rehabenhet</title>
    <updated>2022-10-01T00:26:06.536000+00:00</updated>
    <content type="html">Är du legitimerad arbetsterapeut med erfarenhet från ortopedisk verksamhet? Vill du tillsammans med oss jobba mot målet att vår arbetsplats ska bli ett föredöme inom framtidens offentliga sjukvård? Då är det kanske dig vi söker. Välkommen till en arbetsplats där du är mitt i livet och samhället. Och gör skillnad varje dag. På riktigt. Om arbetsplatsen...</content>
    <published>2022-09-13T15:48:52+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/27388397</id>
    <title>Agila söker arbetsterapeuter i Västmanland</title>
    <updated>2023-05-03T15:11:56.626000+00:00</updated>
    <content type="html">Vi på Agila har många lediga uppdrag för arbetsterapeuter. Både på heltid och deltid. Vi söker dig som vill kunna styra mer över din egen tid, detta genom att erbjuda hög flexibilitet samt en god ersättning. Bland våra uppdragsgivare finns kommuner, regioner och privata aktörer från norr till söder i Sverige. VI ERBJUDER DIG...</content>
    <published>2023-04-03T13:29:46+00:00</published>
    <municipality>Västerås</municipality>
  </entry>
</feed>
""",
    "utf-8",
)


def get_opensearch_data() -> list:
    file_name = Path(__file__).resolve().parent / "opensearch_data.json"
    with open(file_name) as f:
        data = json.load(f)
    return data
