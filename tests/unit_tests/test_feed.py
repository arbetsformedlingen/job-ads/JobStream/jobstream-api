# -*- coding: utf-8 -*-
import pytest
from jobstream import settings
from jobstream.load_ads.atom_feed import AtomFeed, load_ads_for_feed

from tests.unit_tests.test_resources import feed_data

atom = AtomFeed()


def test_feed_generation():
    opensearch_data = feed_data.get_opensearch_data()
    feed = atom.generate(opensearch_data)
    assert feed == feed_data.expected_feed_response


@pytest.mark.parametrize(
    "source, expected",
    [
        ({"id": 12345, "something_else": None}, f"{settings.JOBSEARCH_HOST}/ad/12345"),
        ({"external_id": 12345, "something_else": None}, f"{settings.JOBSEARCH_HOST}/ad/12345"),
        ({"external_id": 12345, "id": 789, "something_else": None}, f"{settings.JOBSEARCH_HOST}/ad/789"),
        ({"no_external_id": None, "no_id": None, "something_else": None}, None),
    ],
)
def test_ad_uri(source, expected):
    result = atom._get_ad_id_uri(source)
    assert result == expected


description = """Välkommen till oss på Montessoriskolan Skäret!
    Vi söker dig som är legitimerad lärare i fritidshem. Du har behörighet att undervisa i musik eller idrott. För tjänsten krävs ett gediget intresse för barns lärande och lek. På Montessoriskolan Skäret jobbar vi tillsammans för alla våra barns bästa.
    Om tjänsten
    Framförallt jobbar du på vårt fritidshem som har tre avdelningar med sammanlagt 70-talet inskrivna barn, men du undervisar också i musik eller idrott och är knuten till en lågstadieklass som pedagogiskt stöd. Du har egen och gemensam planeringstid som är schemalagd. Vår skola och vårt fritidshem är inne i en spännande utvecklingsprocess där ”tillsammans” är ett  ledord. Att stärka det pedagogiska samarbetet mellan skola och fritidshem är ett viktigt utvecklingsarbete som vi vill att du tar en aktiv del av.
    Om oss
    Vår vision är: ”Redo för framtiden. Trygg i mig – trygg i världen”, och vår vision genomsyrar verksamheten.
    Vi strävar efter att vara den lilla skolan där samarbete, struktur, studiero och fokus på lärande är viktiga delar i det vardagliga arbetet. Alla som arbetar på skolan har till uppgift att vara tydliga vuxna och skapa en god atmosfär och en trygg lärandemiljö. Du har stöd av elevhälsoteam och kollegor i ditt arbete. Vår skola är en föräldrakooperativt driven friskola utan vinstkrav. Alla pengar går till verksamheten. Skolan har 300 elever i åk f-9 uppdelad i två byggnader. Vi fyllde 25 år förra året och kön till vår skola är lång. Det är nära till staden men också till skog och hav.
    Om dig
    Du är legitimerad lärare i fritidshem och har behörighet att undervisa i musik eller idrott. Du är en trygg vuxen som också gillar att leka. Vi tror även att du är en kommunikativ person som har lätt att bygga relationer till elever, vårdnadshavare och kollegor. Med systematik följer du upp elevers lärande och utveckling och arbetar medvetet för framgångsfaktorer i undervisningen. Du tillför entusiasm, är kreativ och kommer ofta med idéer och nya angreppssätt i arbetsrelaterade frågor. Skolan är liten och allas krafter behövs, så det är viktigt att du är bra på att samarbeta, är ansvarstagande, lösningsfokuserad och flexibel. Att har roligt på jobbet är viktigt för dig och du bidrar till att det blir så!
    ÖVRIGT Som anställd på Montessoriskolan Skäret erbjuds du friskvårdsbidrag.
    Tjänsten är en semesteranställning.
    Vi vill ha din ansökan senast den 28 mars.
    Tillträde i augusti 2021. Intervjuer sker löpande så skicka in din ansökan på en gång.
    För att underlätta rekryteringsarbetet ber vi dig att bifoga relevanta intyg och betyg för tjänsten du söker. Tänk på att Skollagen kräver att samtliga medarbetare visar upp ett utdrag ur belastningsregistret och handläggningstiden för detta dokument kan vara lång. Du hittar ansökan om belastningsregister här (https://polisen.se/tjanster-tillstand/belastningsregistret/skola-eller-forskola/).
    Inför denna rekrytering har vi tagit ställning till rekryteringskanaler och därför undanber vi oss ytterligare erbjudanden om kompetensförmedling, annonserings- och rekryteringshjälp.
"""


def test_create_content():
    description = "first sentence.\n second sentence.\n third line is not included"
    result = atom._create_content(description)
    assert result == "first sentence.  second sentence...."


def test_create_content_no_linebreaks():
    desc = "no line breaks in this sentence. And another sentence. Yet another."
    result = atom._create_content(desc)
    assert result == desc + "..."


def test_create_content_no_text():
    desc = ""
    result = atom._create_content(desc)
    assert result == "..."
