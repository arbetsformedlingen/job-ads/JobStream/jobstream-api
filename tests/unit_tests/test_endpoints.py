# -*- coding: utf-8 -*-
from datetime import datetime
from unittest.mock import patch

import pytest
from jobstream import create_app
from jobstream.endpoints import Stream  # noqa: F401


@pytest.fixture()
def app():
    app = create_app()
    app.config.update({"TESTING": True})

    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@patch("jobstream.endpoints.get_snapshot_json")
def test_api_snapshot_get_json(mock_get_snapshot_json, client):
    # Arrange
    mock_get_snapshot_json.return_value = "test-ad-data"

    # Act
    response = client.get("v2/snapshot", headers={"Accept": "application/json"})

    # Assert
    assert response.status_code == 200
    assert response.mimetype == "application/json"

    assert b"test-ad-data" in response.data


@patch("jobstream.endpoints.get_snapshot_jsonlines")
def test_api_snapshot_get_jsonlines(mock_get_snapshot_jsonlines, client):
    # Arrange
    mock_get_snapshot_jsonlines.return_value = "test-ad-data"

    # Act
    response = client.get("v2/snapshot", headers={"Accept": "application/jsonl"})

    # Assert
    assert response.status_code == 200
    assert response.mimetype == "application/jsonl"

    assert b"test-ad-data" in response.data


@patch("jobstream.endpoints.get_stream_json")
def test_api_stream_get_json(mock_get_stream_json, client):
    # Arrange
    mock_get_stream_json.return_value = "test-ad-data"

    # Act
    response = client.get("v2/stream?updated-after=2024-05-29T14:23:41", headers={"Accept": "application/json"})

    # Assert
    assert response.status_code == 200
    assert response.mimetype == "application/json"

    assert b"test-ad-data" in response.data

    mock_get_stream_json.assert_called_once_with(
        {
            "updated-after": datetime(2024, 5, 29, 14, 23, 41),
            "updated-before": None,
            "occupation-concept-id": None,
            "location-concept-id": None,
        }
    )


@patch("jobstream.endpoints.get_stream_jsonlines")
def test_api_stream_get_jsonlines(mock_get_stream_jsonlines, client):
    # Arrange
    mock_get_stream_jsonlines.return_value = "test-ad-data"

    # Act
    response = client.get("v2/stream?updated-after=2024-05-29T14:23:41", headers={"Accept": "application/jsonl"})

    # Assert
    assert response.status_code == 200
    assert response.mimetype == "application/jsonl"

    assert b"test-ad-data" == response.data

    mock_get_stream_jsonlines.assert_called_once_with(
        {
            "updated-after": datetime(2024, 5, 29, 14, 23, 41),
            "updated-before": None,
            "occupation-concept-id": None,
            "location-concept-id": None,
        }
    )


@patch("jobstream.endpoints.load_ads_for_feed")
def test_api_feed_get(mock_load_ads_for_feed, client):
    # Arrange
    mock_load_ads_for_feed.return_value = "test-ad-data"

    # Act
    response = client.get("v2/feed?updated-after=2024-05-29T14:23:41", headers={"Accept": "application/atom+xml"})

    # Assert
    assert response.status_code == 200
    assert response.mimetype == "application/atom+xml"

    assert b"test-ad-data" in response.data

    mock_load_ads_for_feed.assert_called_once_with(
        {
            "updated-after": datetime(2024, 5, 29, 14, 23, 41),
            "updated-before": None,
            "occupation-concept-id": None,
            "location-concept-id": None,
        }
    )
