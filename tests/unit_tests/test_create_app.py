# -*- coding: utf-8 -*-
from jobstream import create_app


def test_create():
    app = create_app()
    assert app.import_name == "jobstream"
