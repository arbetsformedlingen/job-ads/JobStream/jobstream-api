# -*- coding: utf-8 -*-
import time
from unittest.mock import patch

from jobstream.load_ads.snapshot import _snapshot_query


def test_snapshot_query():
    query = _snapshot_query()

    UTC_offset = int(time.localtime().tm_gmtoff / 3600)

    assert query == {
        "query": {
            "bool": {
                "filter": [
                    {"range": {"publication_date": {"lte": f"now/m{UTC_offset:+}H/m"}}},
                    {"range": {"last_publication_date": {"gte": f"now/m{UTC_offset:+}H/m"}}},
                    {"term": {"removed": False}},
                ]
            }
        }
    }
