# -*- coding: utf-8 -*-
from unittest.mock import patch

from jobstream.load_ads.common import calculate_utc_offset


class localtime(object):
    def __init__(self, tm_isdst):
        self.tm_isdst = tm_isdst


@patch("jobstream.load_ads.common.time")
def testcalculate_utc_offset_with_no_dst(time_mock):
    time_mock.daylight = 0
    time_mock.localtime.return_value = localtime(0)
    time_mock.timezone = -3600  # !is_dst use timezone

    offset = calculate_utc_offset()

    assert offset == 1


@patch("jobstream.load_ads.common.time")
def testcalculate_utc_offset_with_no_dst_tz_plus_four(time_mock):
    time_mock.daylight = 0
    time_mock.localtime.return_value = localtime(0)
    time_mock.timezone = -14400  # !is_dst use timezone

    offset = calculate_utc_offset()

    assert offset == 4


@patch("jobstream.load_ads.common.time")
def testcalculate_utc_offset_with_dst(time_mock):
    time_mock.daylight = 1
    time_mock.localtime.return_value = localtime(1)
    time_mock.altzone = -7200  # is_dst use altzone

    offset = calculate_utc_offset()

    assert offset == 2


@patch("jobstream.load_ads.common.time")
def testcalculate_utc_offset_with_dst_tz_plus_four(time_mock):
    time_mock.daylight = 1
    time_mock.localtime.return_value = localtime(1)
    time_mock.altzone = -18000  # is_dst use altzone

    offset = calculate_utc_offset()

    assert offset == 5
