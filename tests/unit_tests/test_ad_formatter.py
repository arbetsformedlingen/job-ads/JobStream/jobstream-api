# -*- coding: utf-8 -*-
import pytest
from jobstream.ad_formatter import _remove_contacts_if_null, format_one_ad

from tests.unit_tests.test_resources.ad_format_test_data import ad_after_formatting, ad_before


def test_full_ad():
    before = ad_before
    expected = ad_after_formatting
    formatted = format_one_ad(before)
    assert formatted == expected


def test_none():
    with pytest.raises(AttributeError):
        format_one_ad(None)


def test_remove_contacts_return_empty_list():
    value_list = [{"contactType": None, "description": None, "email": None, "name": None, "telephone": None}]
    result = _remove_contacts_if_null(value_list)
    assert result == []


def test_remove_contacts():
    value_list = [
        {
            "contactType": None,
            "description": "blabla",
            "email": "test@jobtechdev.se",
            "name": "Testy Testsson",
            "telephone": "+01011122233",
        }
    ]
    result = _remove_contacts_if_null(value_list)
    assert result == value_list
