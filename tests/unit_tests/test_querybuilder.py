# -*- coding: utf-8 -*-
import datetime

from jobstream.load_ads.stream import query_builder


def test_only_date():
    parsed_args = {
        "updated-after": datetime.datetime(2020, 1, 1, 0, 0, 1),
        "updated-before": None,
        "occupation-concept-id": None,
        "location-concept-id": None,
    }
    query = query_builder(parsed_args)
    timestamp_section = query["query"]["bool"]["should"][0]["range"]["timestamp"]
    assert timestamp_section["gte"] == 1577833201000
    assert timestamp_section["lte"] == 32503676401000

    publication_date_section = query["query"]["bool"]["should"][1]["range"]["publication_date"]
    assert publication_date_section["gte"] == 1577833201000
    assert publication_date_section["lte"] == 32503676401000



def format_str(input_str: str) -> str:
    return input_str.replace("{", "").replace("}", "").replace("'", "").strip()


def test_all_parameter_types():
    occupation = "eU1q_zvL_9Rf"
    location = "CifL_Rzy_Mku"
    parsed_args = {
        "updated-after": datetime.datetime(2020, 11, 25, 0, 0),
        "updated-before": datetime.datetime(2020, 11, 30, 0, 0),
        "occupation-concept-id": [occupation],
        "location-concept-id": [location],
    }
    query = query_builder(parsed_args)
    timestamp_section = query["query"]["bool"]["should"][0]["range"]["timestamp"]

    expected_input_gte_ms = 1606258800000
    expected_input_lte_ms = 1606690801000

    assert timestamp_section["gte"] == expected_input_gte_ms

    assert timestamp_section["lte"] == expected_input_lte_ms

    publication_date_section = query["query"]["bool"]["should"][1]["range"]["publication_date"]
    assert publication_date_section["gte"] == expected_input_gte_ms
    assert publication_date_section["lte"] == expected_input_lte_ms

    minimum_should_match = query["query"]["bool"]["minimum_should_match"]
    assert minimum_should_match == 1

    occupation_list = query["query"]["bool"]["filter"][2]["bool"]["should"]
    assert len(occupation_list) == 3
    for item in occupation_list:
        tmp = str(item["term"]).split(":")
        key = format_str(tmp[0])
        value = format_str(tmp[1])
        assert occupation == value
        assert key in [
            "occupation.concept_id.keyword",
            "occupation_field.concept_id.keyword",
            "occupation_group.concept_id.keyword",
        ]

    location_list = query["query"]["bool"]["filter"][3]["bool"]["should"]
    for item in location_list:
        tmp = str(item["term"]).split(":")
        key = format_str(tmp[0])
        value = format_str(tmp[1])
        assert location == value
        assert key in [
            "workplace_address.municipality_concept_id",
            "workplace_address.region_concept_id",
            "workplace_address.country_concept_id",
        ]
    result = {
        "query": {
            "bool": {
                "filter": [
                    {"range": {"publication_date": {"lte": "now/m+1H/m"}}},
                    {"range": {"last_publication_date": {"gte": "now/m+1H/m"}}},
                    {
                        "bool": {
                            "should": [
                                {"term": {"occupation.concept_id.keyword": "eU1q_zvL_9Rf"}},
                                {"term": {"occupation_field.concept_id.keyword": "eU1q_zvL_9Rf"}},
                                {"term": {"occupation_group.concept_id.keyword": "eU1q_zvL_9Rf"}},
                            ]
                        }
                    },
                    {
                        "bool": {
                            "should": [
                                {"term": {"workplace_address.region_concept_id": "CifL_Rzy_Mku"}},
                                {"term": {"workplace_address.country_concept_id": "CifL_Rzy_Mku"}},
                                {"term": {"workplace_address.municipality_concept_id": "CifL_Rzy_Mku"}},
                            ]
                        }
                    },
                ],
                "should": [
                    {"range": {"timestamp": {"gte": expected_input_gte_ms, "lte": expected_input_lte_ms}}},
                    {"range": {"publication_date": {"gte": expected_input_gte_ms, "lte": expected_input_lte_ms}}}
                ],
                "minimum_should_match": 1

            }
        }
    }
    print(result)
