# -*- coding: utf-8 -*-
import json

import pytest
from jobstream.load_ads.common import generator_json, generator_jsonlines

from tests.test_resources.scan_result import scan_result


def test_generator_json():
    result = generator_json(scan_result)
    results = []
    for entry in result:
        results.append(entry)
    ads = [1, 3, 5]
    commas = [2, 4]
    assert results[0] == "["
    for row in commas:
        assert results[row] == ","
    for row in ads:
        ad = results[row]
        assert isinstance(ad, str)
        ad_json = json.loads(ad)
        assert len(ad_json) == 36
    assert results[6] == "]"


def test_generator_json_none():
    result = generator_json(None)
    with pytest.raises(TypeError):
        for x in result:
            pass


def test_generator_jsonlines():
    result = generator_jsonlines(scan_result)
    results = []
    for entry in result:
        results.append(entry)

    ads = [0, 2, 4]
    line_breaks = [1, 3]
    for row in line_breaks:
        assert results[row] == "\n"
    for row in ads:
        ad = results[row]
        assert isinstance(ad, str)
        ad_json = json.loads(ad)
        assert len(ad_json) == 36
