# -*- coding: utf-8 -*-
from jobstream import settings
from jobstream.result_model import AdUrl


def test_without_slash():
    ad_url = AdUrl()
    ad_id = 123
    settings.BASE_PB_URL = "http://abc"
    formatted_url = ad_url.format(ad_id)
    assert formatted_url == "http://abc/123"


def test_with_slash():
    ad_url = AdUrl()
    ad_id = 123
    settings.BASE_PB_URL = "http://abc/"
    formatted_url = ad_url.format(ad_id)
    assert formatted_url == "http://abc/123"
