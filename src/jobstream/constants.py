# -*- coding: utf-8 -*-
API_VERSION = "2.1.0"


RESULT_MODEL = "resultmodel"

# Stream
DATE = "date"
UPDATED_BEFORE_DATE = "updated-before-date"
UPDATED_AFTER = "updated-after"
UPDATED_BEFORE = "updated-before"
MAX_DATE = "3000-01-01T00:00:00"
OCCUPATION_CONCEPT_ID = "occupation-concept-id"
LOCATION_CONCEPT_ID = "location-concept-id"
OCCUPATION_LIST = ["occupation", "occupation_field", "occupation_group"]
LOCATION_LIST = ["region", "country", "municipality"]

DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"

RESTPLUS_SWAGGER_UI_DOC_EXPANSION = "list"
RESTPLUS_VALIDATE = False
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False
result_models = ["pbapi", "simple"]

# Endpoint names
FEED_V2 = "v2/feed"
FEED_LEGACY = "feed"
STREAM_V2 = "v2/stream"
STREAM_LEGACY = "stream"
SNAPSHOT_V2 = "v2/snapshot"
SNAPSHOT_LEGACY = "snapshot"
