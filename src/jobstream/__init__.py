# -*- coding: utf-8 -*-
import logging

from flask import Flask
from flask_cors import CORS

from jobstream import constants
from jobstream.api import api
from jobstream.main_opensearch_client import OpensearchClient

opensearch_client = OpensearchClient()


def create_app():
    # Import all Resources that are to be made visible for the app
    # PyCharm marks them as unused imports, but they are needed
    # noinspection PyUnresolvedReferences
    import jobstream.endpoints  # noqa: F401

    app = Flask(__name__)

    CORS(app)

    app.config.SWAGGER_UI_DOC_EXPANSION = constants.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    app.config.RESTPLUS_VALIDATE = constants.RESTPLUS_VALIDATE
    app.config.RESTPLUS_MASK_SWAGGER = constants.RESTPLUS_MASK_SWAGGER
    app.config.ERROR_404_HELP = constants.RESTPLUS_ERROR_404_HELP

    api.init_app(app)

    logging.info("Created Flask app")
    return app
