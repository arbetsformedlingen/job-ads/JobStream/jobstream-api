# -*- coding: utf-8 -*-
from flask_restx import Api, Namespace, inputs, reqparse

from jobstream import constants
from jobstream.result_model import root_api

api = Api(
    version=constants.API_VERSION,
    title="JobStream",
    description="## An API for retrieving all currently published job ads from the Swedish Public Employment Service\n"
    "**Useful links:**\n"
    "- [JobStream getting started](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobStreamEN.md)\n"
    "- [JobStream Code example](https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example)\n"
    "- [Taxonomy Getting Started](https://arbetsformedlingen.gitlab.io/taxonomy-dev/projects/jobtech-taxonomy/overview.html)\n"
    "- [Taxonomy Atlas](https://atlas.jobtechdev.se)\n",
    default_label="An API for retrieving job ads",
    contact_url="https://forum.jobtechdev.se/c/vara-api-er-dataset/job-stream/25",
    contact="Contact: JobTech Forum",
    license="Ads are licensed under CC0",
    license_url="https://creativecommons.org/publicdomain/zero/1.0/deed.sv",
)

ns_stream = Namespace("Stream", description="Start with a snapshot and keep it up to date with stream")
ns_snapshot = Namespace("Snapshot", description="Start with a snapshot and keep it up to date with stream")
ns_feed = Namespace("Feed", description="An Atom/xml feed of ads")

api.add_namespace(ns_snapshot, "/")
api.add_namespace(ns_stream, "/")
api.add_namespace(ns_feed, "/")

for name, definition in root_api.models.items():
    ns_stream.add_model(name, definition)

stream_query = reqparse.RequestParser()
stream_query.add_argument(
    constants.UPDATED_AFTER,
    type=inputs.datetime_from_iso8601,
    required=True,
)
stream_query.add_argument(constants.UPDATED_BEFORE, type=inputs.datetime_from_iso8601, required=False)
stream_query.add_argument(constants.OCCUPATION_CONCEPT_ID, action="append", required=False)
stream_query.add_argument(constants.LOCATION_CONCEPT_ID, action="append", required=False)

legacy_stream_query = reqparse.RequestParser()
legacy_stream_query.add_argument(
    constants.DATE,
    type=inputs.datetime_from_iso8601,
    required=True,
)
legacy_stream_query.add_argument(constants.UPDATED_BEFORE_DATE, type=inputs.datetime_from_iso8601, required=False)
legacy_stream_query.add_argument(constants.OCCUPATION_CONCEPT_ID, action="append", required=False)
legacy_stream_query.add_argument(constants.LOCATION_CONCEPT_ID, action="append", required=False)


# /snapshot endpoint
snapshot_query = reqparse.RequestParser()
