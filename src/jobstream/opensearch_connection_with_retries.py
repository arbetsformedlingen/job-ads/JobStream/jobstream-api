# -*- coding: utf-8 -*-
import logging
import os
import platform
import sys
import time

import requests
from opensearchpy import OpenSearch, OpenSearchException

from jobstream import settings

log = logging.getLogger(__name__)


def create_opensearch_client_with_retry(
    host,
    port=settings.DB_PORT,
    user=settings.DB_USER,
    password=settings.DB_PWD,
    db_timeout=settings.DB_DEFAULT_TIMEOUT,
):
    start_time = int(time.time() * 1000)
    log.info(f"Creating opensearch client, host: {', '.join(host)}, port: {port} user: {user} ")
    fail_count = 0
    max_retries = settings.DB_RETRIES
    auth = None
    if user and password:
        auth = (user, password)

    for i in range(max_retries):
        try:
            if auth:
                opensearch = OpenSearch(
                    hosts=[{"host": host, "port": settings.DB_PORT} for host in settings.DB_HOST],
                    http_compress=True,  # enables gzip compression for request bodies
                    http_auth=auth,
                    use_ssl=settings.DB_USE_SSL,
                    verify_certs=settings.DB_VERIFY_CERTS,
                    ssl_assert_hostname=False,
                    ssl_show_warn=False,
                    timeout=db_timeout,
                    max_retries=10,
                    retry_on_timeout=True,
                )
            else:
                opensearch = OpenSearch(
                    hosts=[{"host": host, "port": settings.DB_PORT} for host in settings.DB_HOST],
                    http_compress=True,  # enables gzip compression for request bodies
                    use_ssl=settings.DB_USE_SSL,
                    verify_certs=settings.DB_VERIFY_CERTS,
                    ssl_assert_hostname=False,
                    ssl_show_warn=False,
                    timeout=db_timeout,
                    max_retries=10,
                    retry_on_timeout=True,
                )

            # check connection
            info = opensearch.info()
            log.info(f"Successfully connected to opensearch node {info['name']} of cluster {info['cluster_name']}.")
            log.debug(f"opensearch info: {info}")
            log.debug(
                f"opensearch node created and connected after: {int(time.time() * 1000 - start_time)} milliseconds"
            )
            return opensearch

        except (OpenSearchException, Exception, requests.exceptions.BaseHTTPError) as e:
            fail_count += 1
            time.sleep(settings.DB_RETRIES_SLEEP)
            log.warning(f"Failed to connect to opensearch, failed attempt: {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to connect to opensearch after attempts: {fail_count}. Exit!")
                hard_kill()


def hard_kill():
    """
    to shut down uwsgi and exit
    for this to work, it needs to be configured not to restart:
    - die-on-term = true in uwsgi.ini
    - autorestart=false in supervisord.conf
    """
    log.error("(hard_kill)Shutting down...")
    if platform.system() != "Windows":
        # send SIGTERM to kill uwsgi
        os.system("killall -15 uwsgi")  # noqa S605, 607
    sys.exit(1)
