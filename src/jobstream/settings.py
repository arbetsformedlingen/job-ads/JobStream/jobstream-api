# -*- coding: utf-8 -*-
import os

# Opensearch settings
DB_HOST = os.getenv("DB_HOST", "127.0.0.1").split(",")
DB_PORT = os.getenv("DB_PORT", 9200)
DB_USER = os.getenv("DB_USER")
DB_PWD = os.getenv("DB_PWD")
DB_USE_SSL = os.getenv("DB_USE_SSL", "true").lower() == "true"
DB_VERIFY_CERTS = os.getenv("DB_VERIFY_CERTS", "true").lower() == "true"

ADS_ALIAS = os.getenv("ADS_ALIAS", "current_ads-stream")

DB_RETRIES = int(os.getenv("DB_RETRIES", 1))
DB_RETRIES_SLEEP = int(os.getenv("DB_RETRIES_SLEEP", 2))
DB_DEFAULT_TIMEOUT = int(os.getenv("DB_DEFAULT_TIMEOUT", 10))

BASE_PB_URL = os.getenv("BASE_PB_URL", "https://arbetsformedlingen.se/platsbanken/annonser/")

JOBSEARCH_HOST = os.getenv("JOBSEARCH_HOST", "https://jobsearch.api.jobtechdev.se")
