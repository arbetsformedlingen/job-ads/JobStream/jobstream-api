# -*- coding: utf-8 -*-
import datetime
import json
import logging
import time

from opensearchpy.helpers.actions import scan

from jobstream import constants, opensearch_client, settings
from jobstream.ad_formatter import format_one_ad
from jobstream.api import ns_stream
from jobstream.result_model import job_ad

log = logging.getLogger(__name__)


def calculate_utc_offset():
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = -(time.altzone if is_dst else time.timezone)
    return int(utc_offset / 3600) if utc_offset > 0 else 0


offset = calculate_utc_offset()


def get_scan_result(dsl):
    scan_result = scan(opensearch_client(), dsl, index=settings.ADS_ALIAS)
    return scan_result


def _stream_base_query():
    """
    Only include ads that are published at the time this function is called
    :return:
    """
    return {
        "query": {
            "bool": {
                "filter": [
                    {"range": {"publication_date": {"lte": "now/m+%dH/m" % offset}}},
                    {"range": {"last_publication_date": {"gte": "now/m+%dH/m" % offset}}},
                ]
            }
        }
    }


def query_builder(args):
    """
    Builds a query with the parameters that can be used for JobStream
    Used by stream and feed
    """
    since = args.get(constants.UPDATED_AFTER)
    # time span, default is None
    if args.get(constants.UPDATED_BEFORE, None):
        before = args.get(constants.UPDATED_BEFORE)
    else:
        before = datetime.datetime.strptime(constants.MAX_DATE, "%Y-%m-%dT%H:%M:%S")

    # Convert timestamp to epoch time which is used in the 'timestamp' field of the ads
    # all "after" and "before" searches are based on the 'timestamp' field which also is when the ad was last updated
    ts_from = int(since.timestamp()) * 1000
    # add 1 sec to find ad (ms truncation problem)
    ts_to = int(before.timestamp() + 1) * 1000

    query = _stream_base_query()
    query["query"]["bool"]["minimum_should_match"] = 1
    query["query"]["bool"]["should"] = [
        {"range": {"timestamp": {"gte": ts_from, "lte": ts_to}}},
        {"range":
            {
                "publication_date": {
                    "gte": ts_from,
                    "lte": ts_to,
                    "time_zone": "-01:00", #Add time_zone to avoid that ads with 1 hour too old publication_date are returned
                }
            }
        }
    ]
    occupation_concept_ids = args.get(constants.OCCUPATION_CONCEPT_ID)
    if occupation_concept_ids:
        occupation_list = [occupation + "." + "concept_id.keyword" for occupation in constants.OCCUPATION_LIST]
        add_filter_query(query, occupation_list, occupation_concept_ids)

    location_concept_ids = args.get(constants.LOCATION_CONCEPT_ID)
    if location_concept_ids:
        location_list = ["workplace_address." + location + "_concept_id" for location in constants.LOCATION_LIST]
        add_filter_query(query, location_list, location_concept_ids)
    return query

def add_filter_query(query, items, concept_ids):
    # add occupation or location filter query
    # Used by stream and feed
    should_query = []
    for concept_id in concept_ids:
        if concept_id:
            for item in items:
                should_query.append({"term": {item: concept_id}})
    query["query"]["bool"]["filter"].append({"bool": {"should": should_query}})
    return query


@ns_stream.marshal_with(job_ad)
def format_ad(ad_data):
    return format_one_ad(ad_data)


def format_removed_ad(ad_data):
    return {
        "id": str(ad_data.get("id")),
        "removed": ad_data.get("removed"),
        "removed_date": ad_data.get("removed_date"),
        "occupation": ad_data.get("occupation", None),
        "occupation_group": ad_data.get("occupation_group", None),
        "occupation_field": ad_data.get("occupation_field", None),
        "municipality": ad_data.get("workplace_address", {}).get("municipality_concept_id", None),
        "region": ad_data.get("workplace_address", {}).get("region_concept_id", None),
        "country": ad_data.get("workplace_address", {}).get("country_concept_id", None),
    }


def generator_json(scan_result):
    """
    Returns ads as json [{ad1}, {ad2}, {ad3}]
    Used by stream and snapshot
    """
    counter = 0
    yield "["  # start of json list
    for ad in scan_result:
        if counter > 0:
            yield ","  # ads are comma-separated
        yield formatter(ad)
        counter += 1
    log.debug(f"Delivered: {counter} ads as json")
    yield "]"  # end of json list


def generator_jsonlines(scan_result):
    """
    returns ads as newline-delimited stream of json objects
    {ad1}\n{ad2}\n
    Used by stream and snapshot
    """
    counter = 0
    for ad in scan_result:
        if counter > 0:
            yield "\n"  # ads are newline-separated
        yield formatter(ad)
        counter += 1
    log.debug(f"Delivered: {counter} ads as jsonl")


def formatter(ad: dict) -> str:
    """
    Format ads differently depending on if the has is removed or not
    This function is also used by snapshot, where all ads have "removed" == False
    """
    source = ad["_source"]
    if source.get("removed", False):
        return json.dumps(format_removed_ad(source))
    else:
        return json.dumps(format_ad(source))
