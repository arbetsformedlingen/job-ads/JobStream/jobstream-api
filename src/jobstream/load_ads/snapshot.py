# -*- coding: utf-8 -*-
import logging

from .common import _stream_base_query, generator_json, generator_jsonlines, get_scan_result

log = logging.getLogger(__name__)


def get_snapshot_json():
    query = _snapshot_query()
    scan_result = get_scan_result(query)
    return generator_json(scan_result)


def get_snapshot_jsonlines():
    query = _snapshot_query()
    scan_result = get_scan_result(query)
    return generator_jsonlines(scan_result)


def _snapshot_query():
    dsl = _stream_base_query()
    # Snapshot excludes removed ads
    dsl["query"]["bool"]["filter"].append({"term": {"removed": False}})
    return dsl
