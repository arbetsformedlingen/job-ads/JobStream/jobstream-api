# -*- coding: utf-8 -*-
import logging

from .common import generator_json, generator_jsonlines, get_scan_result, query_builder

log = logging.getLogger(__name__)


def get_stream_json(args):
    dsl = query_builder(args)
    scan_result = get_scan_result(dsl)
    return generator_json(scan_result)

def get_stream_jsonlines(args):
    dsl = query_builder(args)
    scan_result = get_scan_result(dsl)
    return generator_jsonlines(scan_result)
