# -*- coding: utf-8 -*-
import logging
from datetime import datetime

from flask import Response, request
from flask_restx import Resource

from jobstream import constants
from jobstream.api import (
    ns_feed,
    ns_snapshot,
    ns_stream,
    snapshot_query,
    stream_query,
    legacy_stream_query,
)
from jobstream.constants import (
    FEED_LEGACY,
    FEED_V2,
    SNAPSHOT_LEGACY,
    SNAPSHOT_V2,
    STREAM_LEGACY,
    STREAM_V2,
)
from jobstream.load_ads.atom_feed import load_ads_for_feed
from jobstream.load_ads.snapshot import get_snapshot_json, get_snapshot_jsonlines
from jobstream.load_ads.stream import get_stream_json, get_stream_jsonlines

log = logging.getLogger(__name__)

example_time = datetime.now().strftime(constants.DATE_FORMAT)

feed_description = """Ads as Atom/XML.
Download all the ads that has been changed (e.g. added, updated, unpublished, published) during requested time interval as an Atom feed.
If you try it out in a web browser, use a short date interval, otherwise the volume of data might cause your browser to hang."""

stream_description = """Download all the ads that has been changed (e.g. added, updated, unpublished, published) during requested time interval.
To get the response formatted as JSON lines make sure to send the correct accept header (application/jsonl).

If you try it out in a web browser, use a short date interval, otherwise the volume of data might cause your browser to hang."""

params_stream_feed = {
    constants.UPDATED_AFTER: "Stream ads changed since datetime. "
    "Accepts datetime as YYYY-MM-DDTHH:MM:SS, "
    "for example %s" % example_time,
    constants.UPDATED_BEFORE: "Stream ads changed before datetime. "
    "Accepts datetime as YYYY-MM-DDTHH:MM:SS. "
    "Optional if you want to set a custom time span. "
    "Defaults to 'now' if not set.",
    constants.OCCUPATION_CONCEPT_ID: "Filter stream ads by one or more occupation concept ids "
    "(from occupation, occupation_group, occupation_field).",
    constants.LOCATION_CONCEPT_ID: "Filter stream ads by one or more location concept ids "
    "(from municipality_concept_id, region_concept_id, country_concept_id).",
}

legacy_params_stream_feed = {
    constants.DATE: "Stream ads changed since datetime. "
    "Accepts datetime as YYYY-MM-DDTHH:MM:SS, "
    "for example %s" % example_time,
    constants.UPDATED_BEFORE_DATE: "Stream ads changed before datetime. "
    "Accepts datetime as YYYY-MM-DDTHH:MM:SS. "
    "Optional if you want to set a custom time span. "
    "Defaults to 'now' if not set.",
    constants.OCCUPATION_CONCEPT_ID: "Filter stream ads by one or more occupation concept ids "
    "(from occupation, occupation_group, occupation_field).",
    constants.LOCATION_CONCEPT_ID: "Filter stream ads by one or more location concept ids "
    "(from municipality_concept_id, region_concept_id, country_concept_id).",
}

responses_stream_feed = {200: "OK", 400: "Bad Request", 500: "Technical error"}

snapshot_description = "Download all the ads currently published in Platsbanken. "
"The intended usage for this endpoint is to make an initial copy of the job ad dataset "
"and then use the stream endpoint to keep it up to date. "
"Avoid using in Swagger, your browser will probably hang"

responses_snapshot = {200: "OK", 500: "Technical error"}


@ns_stream.deprecated
@ns_stream.route(STREAM_LEGACY)
class StreamLegacy(Resource):
    @ns_stream.doc(
        description=f"Deprecated, use the /{STREAM_V2} endpoint\n",
        params=legacy_params_stream_feed,
        responses=responses_stream_feed,
    )
    @ns_stream.expect(legacy_stream_query)
    def get(self, **kwargs):
        args = legacy_stream_query.parse_args()
        log.debug(f"ARGS: {args}")
        switch_legacy_params_to_v2_params(args)

        return Response(get_stream_json(args), mimetype="application/json")


@ns_stream.route(STREAM_V2)
class Stream(Resource):
    @ns_stream.doc(
        description="Ads as JSON or JSON lines.\n" + stream_description,
        params=params_stream_feed,
        responses=responses_stream_feed,
    )
    @ns_stream.produces(["application/json", "application/jsonl"])
    @ns_stream.expect(stream_query)
    def get(self):
        args = stream_query.parse_args()
        log.debug(f"ARGS: {args}")

        mimetype = request.accept_mimetypes.best_match(["application/json", "application/jsonl"], "application/json")

        if mimetype == "application/json":
            return Response(get_stream_json(args), mimetype="application/json")

        if mimetype == "application/jsonl":
            return Response(get_stream_jsonlines(args), mimetype=mimetype)


@ns_snapshot.deprecated
@ns_snapshot.route(SNAPSHOT_LEGACY)
class SnapshotLegacy(Resource):
    @ns_snapshot.doc(
        description=f"Deprecated, use the /{SNAPSHOT_V2} endpoint\n",
        responses=responses_snapshot,
    )
    @ns_snapshot.expect(snapshot_query)
    def get(self, **kwargs):
        return Response(get_snapshot_json(), mimetype="application/json")


@ns_snapshot.route(SNAPSHOT_V2)
class Snapshot(Resource):
    @ns_snapshot.doc(description="Ads as JSON or JSON lines.\n" + snapshot_description, responses=responses_snapshot)
    @ns_snapshot.produces(["application/json", "application/jsonl"])
    @ns_snapshot.expect(snapshot_query)
    def get(self, **kwargs):
        mimetype = request.accept_mimetypes.best_match(["application/json", "application/jsonl"], "application/json")

        if mimetype == "application/json":
            return Response(get_snapshot_json(), mimetype="application/json")

        if mimetype == "application/jsonl":
            return Response(get_snapshot_jsonlines(), mimetype=mimetype)


@ns_stream.deprecated
@ns_feed.route(FEED_LEGACY)
class FeedLegacy(Resource):
    @ns_feed.doc(
        description=f"Deprecated, use the /{FEED_V2} endpoint\n",
        params=legacy_params_stream_feed,
        responses=responses_stream_feed,
        content_type="application/xml",
    )
    @ns_feed.expect(legacy_stream_query)
    def get(self, **kwargs):
        args = legacy_stream_query.parse_args()
        log.debug(f"ARGS: {args}")
        switch_legacy_params_to_v2_params(args)

        return Response(
            response=load_ads_for_feed(args),
            status=200,
            mimetype="application/atom+xml",
        )


@ns_feed.route(FEED_V2)
class Feed(Resource):
    @ns_feed.doc(
        description=feed_description,
        params=params_stream_feed,
        responses=responses_stream_feed,
    )
    @ns_feed.produces(["application/atom+xml"])
    @ns_feed.expect(stream_query)
    def get(self, **kwargs):
        args = stream_query.parse_args()
        log.debug(f"ARGS: {args}")
        return Response(
            response=load_ads_for_feed(args),
            status=200,
            mimetype="application/atom+xml",
        )

def switch_legacy_params_to_v2_params(args):
    '''
    Switches values from old parameters (legacy names) to new v2-parameters.
    :return: args with updated parameters
    '''
    args[constants.UPDATED_AFTER] = args.get(constants.DATE, None)
    args[constants.UPDATED_BEFORE] = args.get(constants.UPDATED_BEFORE_DATE, None)
    args.pop(constants.DATE)
    args.pop(constants.UPDATED_BEFORE_DATE)