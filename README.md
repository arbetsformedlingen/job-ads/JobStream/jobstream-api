# JobStream API

* ___JobStream___ is meant to simplify keeping a local copy of currently published ads in Platsbanken by subscribing to
  changes.

## Requirements

* Python 3.12.0 or later
* Access to a host or server running Opensearch version 2.6.0 or later

## Environment variables

The application is entirely configured using environment variables.
Default values are provided with the listing.

### Application configuration

| Variable Name | Default Value      | Description                                             |
|---------------|--------------------|---------------------------------------------------------|
| DB_HOST       | 127.0.0.1          | Specifies which opensearch host to use for searching.   |
| DB_PORT       | 9200               | Port number to use for opensearch                       |
| ADS_ALIAS     | current_ads-stream | Specifies which OpenSearch alias to use for getting ads |
| DB_USER       |                    | Sets username for opensearch (no default value)         |
| DB_PWD        |                    | Sets password for opensearch (no default value)         |

### Flask configuration

| Variable Name | Default Value | Description                                     |
|---------------|---------------|-------------------------------------------------|
| FLASK_APP     |               | The name of the application. Set to "jobstream" |
| FLASK_ENV     |               | Set to "development" for development.           |

## Running the service

### Python version

Python 3.12.0 or later is required

### Installing dependencies

Dependency management is done with Poetry. You need to install it before continuing.
<https://python-poetry.org/docs>

When that is done, you can install the dependencies with:
`python -m poetry install`
For development and testing, you need to install additional dependencies:
`python -m poetry install --only dev, pytest` which installs dependencies from the `[tool.poetry.group.dev.dependencies]` and `[tool.poetry.group.pytest.dependencies]` groups in pyproject.toml.
The reason for separate installations is to avoid installing unnecessary code in the Docker image.

### Pre-commit checks

[pre-commit](https://pre-commit.com/)
The checks are defined in the file `.pre-commit-config.yaml` and you need to install the git hooks with `pre-commit install`.
This installs the code for linters, formatters etc and sets up the pre-commit hooks.
Whenever possible, the configuration for the checks is in `pyproject.toml`.
Some time-consuming checks are done pre-push instead of pre-commit.
If you want to run all checks without committing, the command is: `pre-commit run` for changed files, or `pre-commit run --all-files` for all files.
If you absolutely need to commit code that doesn't pass the checks, add `--no-verify` to the commit command, e.g.
`git commit --no-verify -m "commit message"`
Annotations check is enabled, this means that if a Python file is modified, it must also have type hints (parameters and return types), otherwise the pre-commit linter will fail.

### Start jobstream

To start up the application, set the appropriate environment variables as described above. Then run the following
commands.

    export FLASK_APP=jobstream
    export FLASK_ENV=development
    flask run

When running from your IDE, select `<path>/jobstream-api/jobstream.py` and add environment variables in the
configuration

Go to <http://127.0.0.1:5000> to access the swagger API.

## Tests

### Run unit tests

    pytest tests/unit_tests

### Run integration tests

When running integration tests, an actual application is started,
so you need to specify environment variables for opensearch in order for it to run properly.

    pytest tests/integration_tests

### Run API tests

When running api tests, the application must be started.

    pytest tests/api_tests

### Smoke tests

A subset of the api tests to test broadly but not with all test cases. Used to save time, if smoke tests fail, there is
no
point in running the entire test suite.

    pytest -m smoke tests/api_tests

### Test coverage

<https://pytest-cov.readthedocs.io/en/latest/>
Example on how to run all tests (unit, integration and api tests) with coverage
$ pytest --cov=stream tests/

## Documentation

Check the `/docs`folder

