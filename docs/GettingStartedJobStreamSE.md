# Stream API för job ads - kom igång

*[Om du vill komma igång med att skriva kod för att anropa våra API:er, gå till https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples]*

Syftet med denna text är att guida dig genom vad du ser i [Swagger-UI](https://jobstream.api.jobtechdev.se) för att hjälpa dig att ladda ner alla jobbannonser som publiceras på Arbetsförmedlingen. Detta är mycket enklare än att använda sök-API:et för att ladda ner allt innehåll. Jobstream API:et är bäst att använda för att indexera annonserna efter deras id i din backend.

## Innehållsförteckning

* [Endpoints](#endpoints)
* [Results](#results)
* [Errors](#errors)

## Introduktion

JobStream har följande endpoints:

* [Ström](#ström) - returnerar alla aktiva annonser som har uppdaterats efter en given tidpunkt eller mellan två tidsstämplar.

* [Snapshot](#snapshot) - returnerar alla aktiva annonser.

* Atom - returnerar annonser som Atom-feed.

Det mest uppenbara användningsområdet för JobStream är att ha en aktuell kopia av alla för närvarande publicerade annonser i en lokal databas. En bra startpunkt skulle vara att ladda ner en /snapshot för att få alla för närvarande öppna annonser och sedan lägga till det genom att göra upprepade anrop till /stream slutpunkten för att hålla datasetet aktuellt.

![JobStream database workflow](https://github.com/Jobtechdev-content/Jobstream-content/blob/develop/JobStream.png?raw=true)

Enklaste sättat att prova APIet är genom att besöka [swagger](https://jobstream.api.jobtechdev.se/).

## Endpoints

Exemplen nedan visar bara URIn. Om du föredrar att använda curl, skriv:

```shell
curl "{URL}" -H "accept: application/json"
```

`/stream` och `/snapshot` endpointsen stödjer både JSON och JSON lines som utdataformat. Se till att lägga till rätt accept header i dina förfrågningar.

### Stream

```
/stream
```

Endpointen `/stream` ger dig alla ändringar i datamängden med annonser som för närvarande är öppna för ansökan. Det innebär att 3 olika typer av händelser kan förekomma, nya annonser, borttagning av annonser och uppdateringar av annonser. Genom att använda `/stream` kan du snabbt uppdatera din datamängd med små och effektiva anrop och svar.

Du måste ange en tidsstämpel som bestämmer starttiden från när du vill ha dina annonser. Att skicka en tid från igår klockan halv tre ger dig alla ändringar mellan den tidsstämpeln och nu. Formatet för tidsstämpeln är ÅÅÅÅ-MM-DDTHH:MM:SS. Hastighetsbegränsningen är en förfrågan per minut. För att hålla en realtidskopia av alla annonser från Arbetsförmedlingen bör du göra ett anrop varje minut:

```
/stream?date=2020-09-24T10:00:00
```

Där den angivna tiden alltid är now() minus en minut.
Alternativt, för mer exakthet, kan du ange ett datumintervall för att få alla ändringar av annonser (som för närvarande är öppna för ansökan) inom det intervallet:

```
/stream?date=2020-09-23T10:00:00&updated-before-date=2020-09-24T10:00:00
```

Om du vill filtrera annonser för en delmängd av arbetsmarknaden kan du använda occupation_ids som filter. Det innebär att du kan filtrera dina resultat för geografiska områden: land (country), län (region), kommun (municipality). Du kan också filtrera med hjälp av concept_ids för occupation_field, occupation_group och occupation_name enligt JobTech Taxonomy.

```
/stream?date=2020-09-22T13:11:06&occupation-concept-id=MVqp_eS8_kDZ&location-concept-id=oDpK_oZ2_WYt
```

Både geografiska och yrkesmässiga filter är hierarkiska, så den högsta nivån du filtrerar på är det du kommer att få. Om du ger flera av samma nivå kommer du att få annonser som motsvarar alla dessa. Om du filtrerar både efter yrke och geografi kommer de att ha ett OCH-förhållande. Det betyder att du bara kommer att få annonser för Copywriter i Kalmar län om du anger båda dessa filter.

### Snapshot

```
/snapshot
```

Snapshot-endpointen ger dig jobbannonserna som för närvarande är öppna för ansökan. Utan specifika händelser för borttagningar eller uppdateringar som i stream-endpointen. Inga parametrar behövs. Utdatafilen är cirka 300 MB, inte rekommenderad för användning i en webbläsare.

### Kodexempel

Kodexempel för att komma åt API:et finns i 'getting-started-code-examples'-repositoryt på Github:
<https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples>

## Resultat

Resultaten av dina förfrågningar kommer att vara i [JSON](https://en.wikipedia.org/wiki/JSON)-format. Vi kommer inte att försöka förklara attributen för annonsobjekten i detta dokument. Istället har vi beslutat att inkludera detta i datamodellen som du kan hitta i vår [Swagger GUI](https://jobsearch.api.jobtechdev.se) för JobSearch.

Lyckade förfrågningar kommer att ha en statuskod på 200 och ge dig en resultatuppsättning som består av annonshändelser som inträffade inom den tidsperiod du skickat in.

Dessa händelser kan vara av 3 olika typer: nya annonser, uppdaterade annonser och borttagna annonser. Nya och uppdaterade annonser kommer att se likadana ut, det enda som skiljer dem åt är att en uppdaterad annons har ett ID som redan finns i databasen för öppna annonser.

Ett objekt vid borttagning ser ut så här:

 {
     "id": 24233930,
     "removed": true,
     "removed_date": "2020-09-22T14:57:55"
     "occupation": "Y3QA_5pk_uXd",
     "occupation_group": "jY19_knH_MJp",
     "occupation_field": "NYW6_mP6_vwf",
     "municipality": "aYA7_PpG_BqP",
     "region": "CifL_Rzy_Mku",
     "country": "i46j_HmG_v64"
   }

Dessa är vanligtvis grupperade tillsammans i din resultatuppsättning, så om din förfrågan har en större tidsperiod än några minuter kan du behöva bläddra för att se faktiska jobbannonser.

## Fel

Misslyckade förfrågningar kommer att returnera antingen status 400 (Bad Request) eller 500 (Internal Server Error) beroende på typ av fel.
