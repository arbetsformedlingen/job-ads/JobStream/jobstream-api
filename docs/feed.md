# Atom endpoint in JobStream: /v2/feed-atom (rss/Atom)

There is a new endpoint in JobStream for showing ads  in [Atom-format](https://en.wikipedia.org/wiki/Atom_%28Web_standard%29)
The accepted parameters are the same as in JobStream. Only the `date` param ('updated after') is mandatory.

## Important differences

- Response is xml, not json
- Only selected fields from the ad is part of the xml feed.
- The description text is truncated.

## xml response

```xml
<?xml version='1.0' encoding='UTF-8'?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <id>https://jobstream.api.jobtechdev.se/feed</id>
  <title>JobStream Feed</title>
  <updated>2021-03-24T12:56:57.863000+00:00</updated>
  <link href="https://jobstream.api.jobtechdev.se" rel="self"/>
  <generator uri="https://jobstream.api.jobtechdev.se" version="2.0.0">JobTech</generator>
  <subtitle>Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen</subtitle>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/24649209</id>
    <title>Legitimerad arbetsterapeut till VC Gagnef</title>
    <updated>2021-03-24T12:56:57.863000+00:00</updated>
    <content type="html">Hos oss på Region Dalarna får du ett meningsfullt arbete där du kan göra skillnad. Tillsammans arbetar vi för ett hälsosamt Dalarna!...</content>
    <published>2021-03-24T12:56:57+00:00</published>
    <municipality>Gagnef</municipality>
  </entry>
  <entry>
    <id>https://jobsearch.api.jobtechdev.se/ad/24647199</id>
    <title>Flexibel arbetsterapeut för sommaruppdrag</title>
    <updated>2021-03-23T18:11:29.401000+00:00</updated>
    <content type="html">AGILA SÖKER ARBETSTERAPEUTER  Agila söker en arbetsterapeuter som är intresserade av arbete över vår och sommar i olika kommuner runtom i landet. Omfattning kan röra sig om 50-100%....</content>
    <published>2021-03-23T18:10:19+00:00</published>
    <municipality>Stockholm</municipality>
  </entry>
  ...
</feed>
```

Note that the `id` in the xml feed is the url to the ad/id enpoint in JobSearch where you can collect the entire ad in json-format.

## Feedback

If you have feedback, please let us know in our [forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/20).
