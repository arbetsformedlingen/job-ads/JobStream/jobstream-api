# Testing of JobSearch-APIS

# Testing

This document describes

- How to load test data
- What different kinds of tests there are in the project
- Configuration
- How to run tests

## But first

Read `README.md`
Commands and paths are for Windows.

## Test data for api tests

The api tests are dependent on a specific set of ads.
Pro:

- easy to detect changes in search results
  Con:
- If a change in the search result happens and it's expected, the tests needs updating with the new number of expected
  ads.

# Test data for JobSearch and JobStream

This is loaded from an api that has the same endpoints as where "live ads" are collected, but it has ads that the
api tests are matched against.

Importing ads into an OpenSearch index is done
with [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers)
The ad source
is [JobSearch Ad Moch API](https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api) which
is deployed in OpenShift testing cluster, but can also be run locally.

| env var               | Value                                                                                                                          |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------|
| LA_FEED_URL           | https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/andradeannonser/              |
| LA_DETAILS_URL        | https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/annonser/                     |
| LA_BOOTSTRAP_FEED_URL | https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/sokningar/publiceradeannonser |

or with JobSearch Ad Mock API running locally:

| env var               | Value                                               |
|-----------------------|-----------------------------------------------------|
| LA_FEED_URL           | http://127.0.0.1:5000/andradeannonser/              |
| LA_DETAILS_URL        | http://127.0.0.1:5000/annonser/                     |
| LA_BOOTSTRAP_FEED_URL | http://127.0.0.1:5000/sokningar/publiceradeannonser |

Use the alias `*-stream* created by Jobsearch-importers when starting the api.
The ads are loaded from an api that has the same endpoints as where "live ads" are collected, but it has 5029 ads that
the api tests are matched against.

# Run tests

The different kinds of tests have different dependencies.

| command                           | Type of tests                      | Dependencies / Configuration |
|-----------------------------------|------------------------------------|------------------------------|
| `pytest tests\unit_tests`         | unit tests                         | None                         |
| `pytest tests\integration_tests`  | tests that need the taxonomy index | OpenSearch                   |
| `pytest tests\api_tests`          | api tests                          | OpenSearch, API              |
| `pytest tests`                    | all tests                          | OpenSearch, API              |
| `pytest -m smoke tests`           | a selection of tests               | OpenSearch, API              |
| `pytest -m smoke tests\api_tests` | a selection of api tests           | OpenSearch, API              |

## Automatic test execution

The only tests that are executed automatically are unit tests when a Docker image is built.

## Test coverage (optional)

Install `pytest-cov` with `poetry install --only test`

Run as part of a pytest command, e.g.
`pytest --cov joblinks tests\unit_tests
`

## PyTest fixtures

`tests/api_tests/conftest.py`
Fixtures for getting the number of ads once and for loading random ads.

## Configuration

`tests/test_resources/test_settings.py`
`TEST_URL = os.getenv('TEST_URL', 'http://127.0.0.1:5000')`
Change if you are running the api on another port than Flask default (5000)

`joblinks/settings.py`
Set OpenSearch connection details if you are using anything other than default values
