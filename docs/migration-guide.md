# Migration guide to JobStream 2

## New in JobStream 2

- New endpoints with ads as JSON lines, allowing you to read them as a stream.
- New naming convention for consistency and to make further development easier
- New parameter names for consistency and ease of understanding
- Deprecation of old endpoints

## Overview of endpoints

| Endpoint            | Data type | notes                 |
|:--------------------|:----------|:----------------------|
| /stream             | json      | deprecated            |
| /v2/stream          | json      |                       |
| /snapshot           | json      | deprecated            |
| /v2/snapshot        | json      |                       |
| /feed               | Atom/xml  | deprecated            |
| /v2/feed-atom       | Atom/xml  |                       |

## Deprecations

The endpoints used in JobStream 1.x are deprecated in favour of a more consistent naming.
They will be available at least until the end of 2024.

## Parameter names

Version 2 will have consistent parameter names for dates, the old parameters will not work.

| Parameter             | Used in version | Status            | Notes          |
|-----------------------|---------------|-------------------|----------------|
| date                  | 1             | deprecated        | /stream, /feed |
| updated-before-date   | 1             | deprecated        | /stream, /feed |
| occupation-concept-id | 1, 2          | unchanged, active |                |
| location-concept-id   | 1, 2          | unchanged, active |                |
| updated-after         | 2             | new, active       | TBD            |
| updated-before        | 2             | new, active       | TBD            |

## Migration to new endpoints delivering ads as JSON

Change the url you are using and change parameter names for dates according to the previous table:
| Old url | New url |
|:----------------------------------------------|:------------------------------------------------------|
| <https://jobstream.api.jobtechdev.se/stream>    | <https://jobstream.api.jobtechdev.se/v2/stream-json>    |
| <https://jobstream.api.jobtechdev.se/snapshot>  | <https://jobstream.api.jobtechdev.se/v2/snapshot-json>  |
| <https://jobstream.api.jobtechdev.se/feed>      | <https://jobstream.api.jobtechdev.se/v2/feed-atom>      |

## Migration to endpoints delivering ads in JSON lines format

This will require some code changes on the client side to handle JSON lines as well as usiing the relevant endpoints.
A working Python example can be found in `docs/demo.py`
