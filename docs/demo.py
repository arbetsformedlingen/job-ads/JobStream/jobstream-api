import json
from datetime import datetime, timedelta

import requests

ten_minutes_back = (datetime.now() - timedelta(minutes=10)).strftime("%Y-%m-%dT%H:%M:%S")
ten_minutes_back = datetime(2021, 3, 1, 0, 0, 0).strftime("%Y-%m-%dT%H:%M:%S")
url = "http://localhost:5000/v2/stream"
mimetype = "application/jsonl"  # request JSON lines formatted data


def process_ad(ad):
    # Do something with the ad
    print(ad["headline"])
    # pass


with requests.get(
    url, stream=True, headers={"accept": mimetype}, params={"date": ten_minutes_back}
) as response:  # N.B. stream=True
    print(response.headers)
    counter = 0
    response.raise_for_status()
    for raw_line in response.iter_lines():  # read line by line from new-line delimited response
        ad = json.loads(raw_line.decode("utf-8"))
        process_ad(ad)
        counter += 1

print(f"Processed {counter} ads")
