# -*- coding: utf-8 -*-
import logging

from jobstream import create_app, opensearch_client
from jobtech.common.customlogging import configure_logging

configure_logging([__name__.split(".")[0], "jobstream"])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + " log level activated")
log.info(f"Starting: {__name__}")

opensearch_client.launch()
app = create_app()

if __name__ == "__main__":
    # Used only when starting this script directly, i.e. for debugging
    app.run(debug=False)
