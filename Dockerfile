FROM docker.io/library/python:3.12.6-slim-bookworm

EXPOSE 8081

ENV TZ=Europe/Stockholm \
    PROC_NR=1 \
    HARAKIRI=120

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . /app

WORKDIR /app

RUN python -m pip install --upgrade pip poetry &&\
    python -m poetry config virtualenvs.create false &&\
    python -m poetry install --with pytest &&\
    python -m poetry run pytest src tests/unit_tests &&\
    rm -rf tests

CMD  uwsgi --http :8081 --manage-script-name --mount /=wsgi:app --ini uwsgi.ini
